# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.1"></a>
## [1.1.1](https://gitlab.com/ironbuzz/reveal-labs/compare/v1.1.0...v1.1.1) (2018-05-10)


### Bug Fixes

* remove loading of google fonts ([79277e4](https://gitlab.com/ironbuzz/reveal-labs/commit/79277e4))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/ironbuzz/reveal-labs/compare/v1.1.0-0...v1.1.0) (2018-05-10)


### Bug Fixes

* optimize assets ([c965564](https://gitlab.com/ironbuzz/reveal-labs/commit/c965564))



<a name="1.1.0-0"></a>
# [1.1.0-0](https://gitlab.com/ironbuzz/reveal-labs/compare/v1.0.1...v1.1.0-0) (2018-05-10)


### Features

* add drift chat ([3164ce8](https://gitlab.com/ironbuzz/reveal-labs/commit/3164ce8))



<a name="1.0.1"></a>
## 1.0.1 (2018-05-10)

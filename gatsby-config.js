// require('dotenv').config()
const config = require('./src/utils/siteConfig')
let contentfulConfig

// If the contentfulConfig can't be found assume the site is being built via Netlify with production environment variables
try {
  contentfulConfig = require('./.contentful.json')
} catch (e) {
  // eslint-disable-next-line
  contentfulConfig = {
    production: {
      spaceId: process.env.SPACE_ID,
      accessToken: process.env.ACCESS_TOKEN,
    },
  }
}

module.exports = {
  plugins: [
    {
      resolve: 'gatsby-plugin-canonical-urls',
      options: {
        siteUrl: config.siteUrl,
      },
    },
    'gatsby-plugin-styled-components',
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-prismjs`,
          },
        ],
      },
    },
    {
      resolve: 'gatsby-source-contentful',
      options:
        process.env.NODE_ENV === 'development'
          ? contentfulConfig.development
          : contentfulConfig.production,
    },
    'gatsby-plugin-netlify',
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography.js`,
        omitGoogleFont: true,
      },
    },
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-next`,
    // {
    //   resolve: 'gatsby-plugin-mailchimp',
    //   options: {
    //     endpoint:
    //       'https://iron-buzz.us17.list-manage.com/subscribe/post?u=b5b9d576cf10499ff8a820a58&id=d43cc8c370', // see instructions section below
    //   },
    // },
    {
      resolve: 'gatsby-plugin-mailchimp-custom',
      options: {
        endpoint:
          'https://iron-buzz.us17.list-manage.com/subscribe/post?u=b5b9d576cf10499ff8a820a58&id=d43cc8c370',
        contactFormEndpoint:
          'https://iron-buzz.us17.list-manage.com/subscribe/post?u=b5b9d576cf10499ff8a820a58&id=7208734522',
      },
    },
  ],
}

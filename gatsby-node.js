const path = require(`path`)

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators

//   const loadPosts = new Promise((resolve, reject) => {
//     graphql(`
//       {
//         allContentfulPost {
//           edges {
//             node {
//               slug
//             }
//           }
//         }
//       }
//     `).then(result => {
//       result.data.allContentfulPost.edges.map(({ node }) => {
//         createPage({
//           path: `blog/${node.slug}/`,
//           component: path.resolve(`./src/templates/post.js`),
//           context: {
//             slug: node.slug,
//           },
//         })
//       })
//       resolve()
//     })
//   })

  const loadPages = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulPage {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      result.data.allContentfulPage.edges.map(({ node }) => {
        if (node.slug !== 'product' && node.slug !== 'blog') {
          // TODO remove when blog is needed
          createPage({
            path: `${node.slug}/`,
            component: path.resolve(
              `./src/templates/${node.slug === 'blog' ? 'blog' : 'page'}.js`
            ),
            context: {
              slug: node.slug,
            },
          })
        }
      })
      resolve()
    })
  })

//   const loadTags = new Promise((resolve, reject) => {
//     graphql(`
//       {
//         allContentfulTag {
//           edges {
//             node {
//               slug
//             }
//           }
//         }
//       }
//     `).then(result => {
//       result.data.allContentfulTag.edges.map(({ node }) => {
//         createPage({
//           path: `tag/${node.slug}/`,
//           component: path.resolve(`./src/templates/tag.js`),
//           context: {
//             slug: node.slug,
//           },
//         })
//       })
//       resolve()
//     })
//   })


  // TODO add blog posts eventually
  // Promise.all([loadPosts,  loadPages, loadTags  ])
  return Promise.all([loadPages])
}

import React from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'
import Img from 'gatsby-image'

import userIcon from '../assets/icons/user.svg'
import calendarIcon from '../assets/icons/calendar.svg'
import tagsIcon from '../assets/icons/tags.svg'

const Post = styled.div`
  margin: 0 0 1em 0;
  width: 100%;
  transition: background 0.2s;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    flex: 0 0 49%;
    margin: 0 0 2vw 0;
  }
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    flex: 0 0 32%;
  }
  a {
    display: flex;
    flex-flow: column;
    height: 100%;
    width: 100%;
    color: ${props => props.theme.colors.base};
    text-decoration: none;
    .gatsby-image-wrapper {
      height: 0;
      padding-bottom: 60%;
      img {
        object-position: top;
      }
    }
    img {
      transition: all 500ms !important;
      &:hover {
        transform: scale(1.02);
      }
    }
  }
  &.featured-post {
    .gatsby-image-wrapper {
      height: 0;
      padding-bottom: 400px;
    }
  }
`

const Metadata = styled.div`
  margin-top: 6px;
  font-weight: 400;
  figure {
    margin: 0 2px 0 0;
    img {
      filter: invert(0.5);
    }
  }
  color: #909090;
`

const Tag = styled.span`
  margin-right: 6px;
  cursor: pointer;
  border-bottom: 1px dashed rgba(192, 192, 192, 0.23);
  &:hover {
    border-bottom: 1px dotted rgba(192, 192, 192, 0.33);
  }
`

const Title = styled.h2`
  font-size: 18px;
  font-weight: 400;
  color: #343434;
`

const Icon = ({ src }) => (
  <figure className="image is-16x16">
    <img src={src} />
  </figure>
)

const UserIcon = () => <Icon src={userIcon} />
const CalendarIcon = () => <Icon src={calendarIcon} />
const TagsIcon = () => <Icon src={tagsIcon} />

const TagsList = ({ tags }) =>
  tags.map(tag => <Tag key={tag.slug}>{tag.title}</Tag>)

const Card = props => {
  return (
    <Post
      className={
        props.isFeatured ? 'column is-12 featured-post' : 'column is-6'
      }
    >
      <div>
        {props.image && (
          <Link to={`/${props.path}/`}>
            <Img sizes={props.image.sizes} backgroundColor={'#eeeeee'} />
          </Link>
        )}
        <Metadata className="level is-mobile is-size-7 is-uppercase">
          <div className="level-left">
            {props.author && (
              <div className="level-item">
                <UserIcon />
                <span>{props.author.fullName}</span>
              </div>
            )}
            {props.date && (
              <div className="level-item">
                <CalendarIcon />
                <span>{props.date}</span>
              </div>
            )}
            {props.tags && (
              <div className="level-item">
                <TagsIcon />
                <TagsList tags={props.tags} />
              </div>
            )}
          </div>
        </Metadata>
        <Link to={`/${props.path}/`}>
          <Title className="subtitle is-uppercase">{props.title}</Title>
        </Link>
      </div>
    </Post>
  )
}

export default Card

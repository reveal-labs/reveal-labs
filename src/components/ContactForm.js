import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import 'whatwg-fetch' // Fetch Polyfill
import { validateEmail, validateCorporateEmail } from '../utils/helpers'

import addToMailchimp from '../../plugins/gatsby-plugin-mailchimp-custom'
import { toast } from 'react-toastify'

/*
  ⚠️ This is an example of a contact form powered with Netlify form handling.
  Be sure to review the Netlify documentation for more information:
  https://www.netlify.com/docs/form-handling/
*/

const Form = styled.form`
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  margin: 0 auto;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  input,
  textarea {
    font-family: inherit;
    font-size: inherit;
    background: none;
    border: none;
    outline: none;
    -webkit-appearance: none;
    -moz-appearance: none;
    background: ${props => props.theme.colors.tertiary};
    color: ${props => props.theme.colors.base};
    border-radius: 2px;
    padding: 1em;
    &:focus {
      outline: none;
    }
    &:required {
      box-shadow: none;
    }
    &::-webkit-input-placeholder {
      color: gray;
    }
    &::-moz-placeholder {
      color: gray;
    }
    &:-ms-input-placeholder {
      color: gray;
    }
    &:-moz-placeholder {
      color: gray;
    }
  }
  &:before {
    content: '';
    background: black;
    height: 100%;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1;
    transition: 0.2s all;
    opacity: ${props => (props.overlay ? '.8' : '0')};
    visibility: ${props => (props.overlay ? 'visible' : 'hidden')};
  }
`

const Name = styled.input`
  margin: 0 0 1em 0;
  width: 100%;
  @media (min-width: ${props => props.theme.responsive.small}) {
    width: 49%;
  }
`

const Email = styled.input`
  margin: 0 0 1em 0;
  width: 100%;
  @media (min-width: ${props => props.theme.responsive.small}) {
    width: 49%;
  }
`

const Message = styled.textarea`
  width: 100%;
  margin: 0 0 1em 0;
  line-height: 1.6;
  min-height: 250px;
  resize: vertical;
`

const Modal = styled.div`
  background: white;
  padding: 2em;
  border-radius: 2px;
  position: fixed;
  min-width: 75%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0 auto;
  z-index: 99;
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  transition: 0.2s all;
  opacity: ${props => (props.visible ? '1' : '0')};
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    min-width: inherit;
    max-width: 400px;
  }
  p {
    line-height: 1.6;
    margin: 0 0 2em 0;
  }
`

const Button = styled.div`
  background: ${props => props.theme.colors.base};
  font-size: 1em;
  display: inline-block;
  margin: 0 auto;
  border: none;
  outline: none;
  cursor: pointer;
  color: white;
  padding: 1em;
  border-radius: 2px;
  text-decoration: none;
  transition: 0.2s;
  z-index: 99;
  &:focus {
    outline: none;
  }
  &:hover {
    background: ${props => props.theme.colors.highlight};
  }
`

const FormContainer = styled.form`
  width: 80%;
  max-width: 800px;
  margin: auto;
  .input {
    width: 100%;
  }
  input,
  textarea {
    border-radius: 0;
  }
  .;
`

const Submit = styled.input`
  background: ${props => props.theme.colors.base} !important;
  color: white !important;
  cursor: pointer;
  transition: 0.2s;
  &:hover {
    background: ${props => props.theme.colors.highlight} !important;
  }
  text-transform: uppercase;
  height: 2.5em;
  letter-spacing: 1px;
  width: 150px;
}
`

const Spinner = styled.div`
  width: 150px;
  height: 40px;
  border: 1px solid black;
  text-align: center;
  display: flex;
  background: #121212;
  justify-content: center;
`

const initialState = () => ({
  name: '',
  email: '',
  phone: '',
  company: '',
  description: '',
  errorMessage: '',
  showModal: false,
})
class ContactForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = initialState()
  }

  handleInputChange = ({ target }) => {
    const { name, value } = target
    const email = name === 'email' ? value : this.state.email

    this.setState({
      [name]: value,
      errorMessage:
        email.length > 0
          ? this.isValidEmail(email)
            ? ''
            : 'Please enter a valid corporate email'
          : '',
    })
  }

  handleSubmit = event => {
    event.preventDefault()
    const fields = Object.entries(this.state).reduce(
      (acc, [key, value]) =>
        ['showModal', 'errorMessage'].includes(key)
          ? acc
          : {
              ...acc,
              [key]: value,
            },
      {}
    )

    if (this.validateFields(fields)) {
      console.log('valid fields')
      this.submitToMailChimp(fields)
    } else {
      console.log('invalid fields')
    }
  }

  submitToMailChimp(fields) {
    this.setState({
      ...initialState(),
      status: `submitting`,
    })
    const { email, name, phone, company, description } = fields

    addToMailchimp(
      email,
      {
        fname: name,
        phone,
        compname: company,
        descbox: description,
      },
      true
    )
      .then(response => {
        // Mailchimp always returns a 200 response
        // So we check the result for MC errors & failures
        // If the user already subscribed we currently show a success message
        if (
          response.result === `success` ||
          response.msg.includes('is already subscribed')
        ) {
          this.onFormSuccess()
        } else {
          this.setState({
            status: `error`,
            errorMessage: response.msg.substring(4),
          })
        }
      })
      .catch(() => {
        // unnecessary because Mailchimp only ever
        // returns a 200 status code
        // TODO: revert to fallback form
        this.setState({ errorMessage: ``, status: `success` })
        this.props.onCloseModal(`success`)
      })
  }

  onFormSuccess() {
    toast.success('The Demo has been requested successfully.', {
      autoClose: 5000,
      hideProgressBar: true,
      draggable: true,
      position: toast.POSITION.TOP_CENTER,
      draggablePercent: 60,
    })

    this.setState({
      ...initialState(),
      status: `success`,
    })
  }

  validateFields(fields) {
    return Object.entries(fields).every(([key, value]) => {
      if (!value) {
        this.setState({
          errorMessage: 'Please fill out all fields',
        })
        return false
      }

      if (key === 'email' && !this.isValidEmail(value)) {
        this.setState({
          errorMessage: 'Please enter a valid corporate email',
        })
        return false
      }

      this.setState({
        errorMessage: '',
      })

      return true
    })
  }

  invalidFields(fields) {
    return Object.entries(fields).filter(([key, value]) => {
      if (!value) {
        return true
      }

      if (key === 'email') {
        if (!this.isValidEmail(value)) {
          return true
        }
      }
    })
  }

  isValidEmail(email) {
    return validateEmail(email) && validateCorporateEmail(email)
  }

  closeModal = () => {
    this.setState({ showModal: false })
  }

  render() {
    const {
      name,
      email,
      phone,
      company,
      description,
      errorMessage,
      status,
    } = this.state

    return (
      <FormContainer
        name="contact"
        onSubmit={this.handleSubmit}
        data-netlify="true"
        data-netlify-honeypot="bot"
      >
        <input type="hidden" name="form-name" value="contact" />
        <p hidden>
          <label>
            Don’t fill this out:{' '}
            <input name="bot" onChange={this.handleInputChange} />
          </label>
        </p>
        <div className="columns is-multiline">
          <div className="column is-6 control">
            <input
              name="name"
              type="text"
              placeholder="Name"
              value={name}
              onChange={this.handleInputChange}
              className="input"
            />
          </div>
          <div className="column is-6 control">
            <input
              name="phone"
              type="text"
              placeholder="Phone"
              value={phone}
              onChange={this.handleInputChange}
              className="input"
            />
          </div>
          <div className="column is-6 field">
            <div className="control">
              <input
                name="email"
                type="text"
                placeholder="Email"
                value={email}
                onChange={this.handleInputChange}
                className="input"
              />
            </div>
          </div>
          <div className="column is-6 control">
            <input
              name="company"
              type="text"
              placeholder="Company Name"
              value={company}
              onChange={this.handleInputChange}
              className="input"
            />
          </div>
          <div className="column is-12 control">
            <textarea
              name="description"
              placeholder="Description"
              value={description}
              onChange={this.handleInputChange}
              className="textarea"
            />
          </div>
        </div>
        <div className="level">
          <div className="level-left has-text-danger">
            {errorMessage && <span>{errorMessage}</span>}
          </div>
          <div className="level-right">
            <div className="control has-text-right">
              {status === 'submitting' ? (
                <Spinner>
                  <div className="spinner">
                    <div className="rect1" />
                    <div className="rect2" />
                    <div className="rect3" />
                    <div className="rect4" />
                    <div className="rect5" />
                  </div>
                </Spinner>
              ) : (
                <Submit
                  className="button"
                  name="submit"
                  type="submit"
                  value="Submit"
                />
              )}
            </div>
          </div>
        </div>
      </FormContainer>
    )
  }
}

ContactForm.propTypes = {
  data: PropTypes.object,
}

export default ContactForm

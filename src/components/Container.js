import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.section`
  margin: 0 auto;
  max-width: ${props => props.theme.sizes.maxWidth};
  padding: 3em 1.5em 2em;
  display: flex;
  p {
    margin: 0 0 30px;
    color: #7e7e7e;
    line-height: 30px;
  }
`

const Container = props => {
  return <Wrapper>{props.children}</Wrapper>
}

export default Container

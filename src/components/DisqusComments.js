import React, { Component } from 'react'
import ReactDisqusComments from 'react-disqus-comments'

class DisqusComments extends Component {
  handleNewComment(comment) {
    console.log(comment.text)
  }
  render() {
    const { shortname, post } = this.props
    return (
      <ReactDisqusComments
        shortname={shortname}
        identifier={'something-unique-12345'}
        title="Example Thread"
        url="http://www.example.com/example-thread"
        category_id="123456"
        onNewComment={this.handleNewComment}
      />
    )
  }
}

export default DisqusComments

import React from 'react'
import styled from 'styled-components'

import { siteTitle } from '../utils/siteConfig'
import { Link } from 'react-router-dom'

import revealImg from '../assets/images/reveal-labs-negative-200.png'

const Wrapper = styled.footer`
  padding-bottom: 3rem;
  background: #1b1b1b;
  img {
    margin-bottom: 6px;
  }
  .footer-menu {
    margin: 0;
    li {
      border-bottom: 1px solid #333;
      line-height: 46px;
      list-style-type: none;
      transition: 6s;
      &:hover {
        border-bottom-color: #17a0e8;
      }
    }
  }
  a {
    position: relative;
    color: silver;
    border-bottom: 1px solid rgba(0, 0, 0, 0);
    transition: 1.6s;
    &:hover {
      span {
        color: #17a0e8;
        transform: rotate(360deg);
      }
    }
    span {
      right: -20px;
      position: absolute;
      transition: all 1s ease;
      color: rgba(0, 0, 0, 0);
    }
  }
`

const scrollIfHome = () => {
  if (window.location.pathname === '/') {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    })
  }
}

const Footer = () => (
  <Wrapper className="footer">
    <div className="container">
      <div className="content">
        <div className="columns has-text-centered-mobile">
          <div className="column is-3">
            <img src={revealImg} />
            <div>
              Copyright © {new Date().getFullYear()} {siteTitle}
            </div>
          </div>
          <div className="column">
            <div className="columns">
              <div className="column">
                <Link to="/" onClick={scrollIfHome}>
                  Home <span>→</span>
                </Link>
              </div>

              <div className="column">
                <Link to="/product">
                  Product <span>→</span>
                </Link>
              </div>

              <div className="column">
                <Link to="/about">
                  About <span>→</span>
                </Link>
              </div>

              <div className="column">
                <Link to="/contact">
                  Contact Us <span>→</span>
                </Link>
              </div>

              <div className="column">
                <Link to="/privacy_policy">
                  Privacy Policy <span>→</span>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </Wrapper>
)

export default Footer

import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'

import RequestDemo from './RequestDemo'
import NavBar from './NavBar'
import { TagLink } from '../styles/theme'

const HeroContainer = styled.section`
  @media (max-width: ${props => props.theme.responsive.mobile}) {
    &.is-large {
      .hero-body {
        height: 100vh !important;
        &:before {
          height: 100vh !important;
        }
        .container {
          height: 100%;
          display: flex;
          flex-direction: column-reverse;
          padding-bottom: 8px;
        }
      }
    }
    .hero-body {
      background-position: 26% 52px;
      background-size: 400%;
      background-repeat: no-repeat;
      background-color: black;

      .hero-text {
        h1 {
          font-size: 22px;
          margin-bottom: 26px !important;
          line-height: 1.4em;
        }
        .subtitle {
          font-size: 16px;
        }
        .title,
        .subtitle {
          text-align: center;
          width: 100%;
        }
        #request-demo {
          text-align: center;
          margin: auto;
          width: 80%;
          a {
            width: 100%;
          }
          .column {
            padding-bottom: 0;
          }
        }
      }
    }
    &.product-header {
      .hero-body {
        background-position: 11% -8%;
        height: 99vh !important;
        .title {
          text-shadow: 1px 0px 0px rgba(0, 0, 0, 0.6);
        }
      }
      .tint {
        opacity: 1;
      }
    }
  }
`

const HeroBody = styled.div`
  background-position: 50% 50%;
  background-size: cover;
  display: flex;
  align-items: center;
  padding-top: 0 !important;
  padding-bottom: 0 !important;
  .container {
    margin-top: 8%;
  }

  .title,
  .subtitle {
    color: white !important;
    text-align: left;
  }

  .title {
    text-transform: uppercase;
    margin-bottom: 40px !important;
    line-height: 1.2em;
  }

  .subtitle {
    margin-bottom: 40px !important;
  }

  .is-medium {
    #request-demo {
      max-width: 600px;
      margin: auto;
    }
  }
`

const DividerLine = styled.hr`
  background-color: #d3d2d3;
  width: 120%;
  margin-left: -10%;
`
const TagsListDiv = styled.div`
  margin-bottom: 12px;
  a {
    &:hover {
      color: #fff !important;
    }
  }
`

const LargeHeader = ({ title, subTitle, showRequestDemoLink }) => (
  <div className="columns">
    <div className="column is-hidden-mobile">&nbsp;</div>
    <div className="column hero-text">
      <h1 className="title is-3 has-text-weight-normal">{title}</h1>
      <h2 className="subtitle is-hidden-mobile is-5">{subTitle}</h2>
      {showRequestDemoLink && <RequestDemo />}
      <DividerLine className="is-hidden-tablet" />
      <h2 className="subtitle is-hidden-tablet  is-5">{subTitle}</h2>
    </div>
  </div>
)

const TagsList = ({ tags }) =>
  Array.isArray(tags) && tags.length > 0 ? (
    <TagsListDiv>
      {tags.map(tag => (
        <TagLink key={tag.id} className="button is-rounded is-small">
          <Link to={`/tag/${tag.slug}/`}>{tag.title}</Link>
        </TagLink>
      ))}
    </TagsListDiv>
  ) : null

const PostHeader = ({ post, title }) => {
  return (
    <div className="has-text-centered hero-text">
      <TagsList tags={post.tags} />
      <h1 className="title is-3 has-text-weight-normal has-text-centered">
        {title}
      </h1>
      <h2 className="subtitle has-text-centered">
        {post.author && <Fragment>By {post.author.fullName} | </Fragment>}
        {post.publishDate}
      </h2>
    </div>
  )
}

const DefaultHeader = ({ title, subTitle, showRequestDemoLink }) => (
  <div className="has-text-centered hero-text is-medium">
    <h1 className="title is-3 has-text-weight-normal has-text-centered">
      {title}
    </h1>
    <h2 className="subtitle has-text-centered">{subTitle}</h2>
    {showRequestDemoLink && <RequestDemo />}
  </div>
)

class Header extends Component {
  render() {
    if (typeof window !== `undefined`) {
      window.location.href = 'https://www.f2f.ai/';
    }
     
    const {
      customClass,
      image,
      isBlogPost,
      isLarge,
      post,
      showRequestDemoLink,
      subTitle,
      title,
    } = this.props

    const showLargeHeader = isLarge && !isBlogPost
    const showBlogHeader = isBlogPost
    const showDefaultHeader = !isLarge && !isBlogPost
    return (
      <div>
        <HeroContainer
          className={`hero is-white ${isLarge ? 'is-large' : 'is-medium'} ${
            isBlogPost ? 'post-header' : ''
          } ${customClass || ''}`}
        >
          <NavBar />
          <HeroBody
            className="hero-body"
            style={
              image
                ? { backgroundImage: `url(${image})` }
                : { backgroundColor: 'black' }
            }
          >
            {customClass === 'product-header' && <span className="tint" />}
            <div className="container has-text-centered">
              {showLargeHeader && (
                <LargeHeader
                  title={title}
                  subTitle={subTitle}
                  showRequestDemoLink={showRequestDemoLink}
                />
              )}
              {showBlogHeader && <PostHeader title={title} post={post} />}
              {showDefaultHeader && (
                <DefaultHeader
                  title={title}
                  subTitle={subTitle}
                  showRequestDemoLink={showRequestDemoLink}
                />
              )}
            </div>
          </HeroBody>
        </HeroContainer>
      </div>
    )
  }
}

export default Header

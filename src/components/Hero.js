import React from 'react'
import Img from 'gatsby-image'
import styled from 'styled-components'
import RequestDemo from './RequestDemo'

const Wrapper = styled.section`
  position: relative;
  min-height: 300px;
  #request-demo {
  }
`
const BgImg = styled(Img)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  z-index: -1;
  min-height: 300px;
  height: auto;
  @media (min-width: ${props => props.theme.responsive.small}) {
    height: ${props => props.height || 'auto'};
  }
  & > img {
    object-fit: ${props => props.fit || 'cover'} !important;
    object-position: ${props => props.position || '50% 50%'} !important;
  }
  &:before {
    content: '';
    background: rgba(0, 0, 0, 0.25);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
  }
`

const Title = styled.h1`
  font-size: 32px;
  text-transform: uppercase;
  font-weight: 600;
  text-align: center;
  color: white;
`

const SubTitle = styled.h3`
  font-size: 20px;
  color: white;
`

const ActionBox = styled.div`
  position: absolute;
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  width: 100%;
  padding: 0 1rem;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 140px;
  & > div {
    flex: 1;
  }
`
// max-width: ${props => props.theme.sizes.maxWidthCentered};

const Hero = props => (
  <Wrapper>
    <BgImg
      height={props.height}
      sizes={props.image.sizes}
      backgroundColor={'#eeeeee'}
    />
    <ActionBox>
      <div>&nbsp;</div>
      <div>
        <Title>{props.title}</Title>
        {props.subTitle && <SubTitle>{props.subTitle}</SubTitle>}
        {props.requestDemo && <RequestDemo />}
      </div>
    </ActionBox>
  </Wrapper>
)

export default Hero

import React, { Component } from 'react'
import styled from 'styled-components'
import RequestDemo from './RequestDemo'

const HeroWrapper = styled.div`
  background-position: 50% 50%;
  background-size: cover;
  display: flex;
  align-items: center;
  padding-top: 0 !important;
  padding-bottom: 0 !important;
  .container {
    margin-top: 8%;
  }

  .title,
  .subtitle {
    color: white !important;
    text-align: left;
  }

  .title {
    text-transform: uppercase;
    margin-bottom: 40px !important;
    text-shadow: 1px 2px 8px rgba(0, 0, 0, 0.5);
    line-height: 1.2em;
  }

  .subtitle {
    margin-bottom: 60px !important;
  }

  @media (max-width: ${props => props.theme.responsive.mobile}) {
    background-position: 26% 52px;
    background-size: 400%;
    background-repeat: no-repeat;
    background-color: black;
    height: 100vh !important;
    .container {
      height: 100%;
      display: flex;
      flex-direction: column-reverse;
      padding-bottom: 8px;
    }
    .hero-text {
      h1 {
        font-size: 22px;
        margin-bottom: 26px !important;
        line-height: 1.4em;
      }
      .subtitle {
        font-size: 16px;
      }
      .title,
      .subtitle {
        text-align: center;
        width: 100%;
      }
      #request-demo {
        text-align: center;
        margin: auto;
        width: 80%;
        a {
          width: 100%;
        }
        .column {
          padding-bottom: 0;
        }
      }
    }
  }
`

const DividerLine = styled.hr`
  background-color: #d3d2d3;
  width: 120%;
  margin-left: -10%;
`
class HeroBody extends Component {
  render() {
    const { image, title, subTitle, showRequestDemoLink, isLarge } = this.props
    return (
      <HeroWrapper
        className="hero-body"
        style={{ backgroundImage: `url(${image})` }}
      >
        <div className="container has-text-centered">
          {isLarge && (
            <div className="columns">
              <div className="column is-hidden-touch">&nbsp;</div>
              <div className="column hero-text">
                <h1 className="title is-3">{title}</h1>
                <h2 className="subtitle is-hidden-mobile is-5">{subTitle}</h2>
                {showRequestDemoLink && <RequestDemo />}
                <DividerLine className="is-hidden-tablet" />
                <h2 className="subtitle is-hidden-tablet  is-5">{subTitle}</h2>
              </div>
            </div>
          )}
          {!isLarge && (
            <div className="has-text-centered">
              <h1 className="title has-text-centered">{title}</h1>
              <h2 className="subtitle has-text-centered">{subTitle}</h2>
              {showRequestDemoLink && <RequestDemo />}
            </div>
          )}
        </div>
      </HeroWrapper>
    )
  }
}

export default HeroBody

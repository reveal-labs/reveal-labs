import React, { Component } from 'react'
import styled from 'styled-components'
import RequestDemo from './RequestDemo'

const Wrapper = styled.div`
  overflow: hidden;
  width: 100%;
  max-height: 600px;
  height: 960px;
  background-position: 50% 50%;
  background-size: cover;
  .hp-container-mid {
    height: 100%;
    display: flex;
    align-items: center;
    width: 80%;

    .Aligner {
      display: flex;
      height: 100%;
      .banner-img {
        flex: 4;
      }
      .banner-slogan {
        display: flex;
        flex-direction: column;
        align-self: center;
        flex: 3;
      }
    }
  }
`

const Title = styled.h1`
  font-size: 32px;
  text-transform: uppercase;
  font-weight: 600;
  color: white;
  line-height: 1.2em;
  letter-spacing: 2px;
`

const SubTitle = styled.h3`
  font-size: 20px;
  color: white;
`

class HomepageBanner extends Component {
  constructor(props) {
    super(props)
    this.image = props.image
    this.title = props.title
    this.subTitle = props.subTitle
    this.showRequestDemoLink = props.showRequestDemoLink
    console.log('img', this.image)
  }

  render() {
    return (
      <Wrapper
        id="fullscreen-banner"
        style={{ backgroundImage: `url(${this.image.file.url})` }}
      >
        <span className="tint" style={{ opacity: 1 }}>
          &nbsp;
        </span>
        <div className="hp-container-mid">
          <div className="Aligner">
            <div className="col-md-5 hidden-xs banner-img">
              <div>&nbsp;</div>
            </div>
            <div className="col-md-7 banner-slogan">
              <Title>{this.title}</Title>
              {this.subTitle && <SubTitle>{this.subTitle}</SubTitle>}
              {this.showRequestDemoLink && <RequestDemo />}
            </div>
          </div>
        </div>
      </Wrapper>
    )
  }
}

export default HomepageBanner

// <Title>{props.title}</Title>
// {props.subTitle && <SubTitle>{props.subTitle}</SubTitle>}
// {props.requestDemo && <RequestDemo />}

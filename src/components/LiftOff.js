import React, { Component } from 'react';
import styled from 'styled-components';

const Lift = styled.div`
position: fixed;
left: 10px;
bottom: 30px;
width: 40px;
height: 40px;
transition: all .3s;
text-align: center;
color: #fff;
background: rgba(0,0,0,.6);
line-height: 40px;
cursor: pointer;
&:hover {
    background: rgba(0,0,0,.4);  
}
`


class LiftOff extends Component {
    scrollToTop = () => {
        window.scroll({
            top: 0, 
            left: 0,
            behavior: 'smooth' 
          });
    }
    render() {
        return (
            <Lift onClick={this.scrollToTop} className="is-hidden-touch">
                <a className="lift-off js-lift-off lift-off_show">⇡</a>
            </Lift>
        );
    }
}

export default LiftOff;
import React, { Component } from 'react'
import Link from 'gatsby-link'
import styled from 'styled-components'
import logo from '../assets/images/logo.png'
import LiftOff from './LiftOff'

const NavWrapper = styled.div`
    &.head52 {
        @media screen and (min-width: 1088px) {
            .navbar {
                height: 52px;
            }
        }
    }
}

`

const Nav = styled.nav`
  transition: height 0.3s, line-height 0.3s, margin 0.3s;
  background-color: white !important;
  .nav-logo {
    max-width: 200px;
    margin-bottom: 0;
  }
  .navbar-item {
    text-transform: uppercase;
  }
  .search-item {
    display: flex;
    align-items: center;
    svg {
      height: 16px;
    }
  }

  .navbar-burger span {
    height: 2px;
    width: 22px;
  }

  @media screen and (min-width: 1088px) {
    height: 100px;
  }
`

const activeLinkStyle = {
  color: '#0E3E59',
}

class NavBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isActive: false,
      expandNavHeight: true,
      showLiftOff: false,
    }
    this.handleScroll = this.handleScroll.bind(this)
    console.log('menu items', props.data)
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll)
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll(event) {
    if (window.pageYOffset > 130 && this.state.expandNavHeight) {
      this.setState({ expandNavHeight: false })
    }

    if (window.pageYOffset <= 130 && !this.state.expandNavHeight) {
      this.setState({ expandNavHeight: true })
    }

    if (window.pageYOffset > 220 && !this.state.showLiftOff) {
      this.setState({ showLiftOff: true })
    }

    if (window.pageYOffset <= 220 && this.state.showLiftOff) {
      this.setState({ showLiftOff: false })
    }
  }

  toggleMenu = () => {
    this.setState(prev => ({ isActive: !prev.isActive }))
  }
  render() {
    const { isActive, expandNavHeight, showLiftOff } = this.state
    return (
      <NavWrapper
        className={expandNavHeight ? 'hero-head' : 'hero-head head52'}
      >
        <Nav className="navbar is-fixed-top" aria-label="dropdown navigation">
          <div className="container">
            <div className="navbar-brand">
              <Link to="/" exact className="navbar-item navbar-item-logo">
                <img className="nav-logo" src={logo} alt="Logo" />
              </Link>
              <span
                className={
                  isActive
                    ? 'navbar-burger burger is-active'
                    : 'navbar-burger burger'
                }
                data-target="navbarMenuHeroA"
                onClick={this.toggleMenu}
              >
                <span>&nbsp;</span>
                <span>&nbsp;</span>
                <span>&nbsp;</span>
              </span>
            </div>
            <div
              id="navbarMenuHeroA"
              className={isActive ? 'navbar-menu is-active' : 'navbar-menu'}
            >
              <div className="navbar-end">
                <Link
                  to="/"
                  exact
                  className="navbar-item"
                  //   activeClassName="is-active"
                  activeStyle={activeLinkStyle}
                >
                  Home
                </Link>

                <Link
                  to="/product/"
                  className="navbar-item"
                  //   activeClassName="is-active"
                  activeStyle={activeLinkStyle}
                >
                  Product
                </Link>
                <Link
                  to="/blog/"
                  className="navbar-item is-hidden"
                  //   activeClassName="is-active"
                  activeStyle={activeLinkStyle}
                >
                  Blog
                </Link>
                <Link
                  to="/contact/"
                  className="navbar-item"
                  //   activeClassName="is-active"
                  activeStyle={activeLinkStyle}
                >
                  Contact Us
                </Link>
                <div className="navbar-item has-dropdown is-hoverable">
                  <Link
                    to="/about/"
                    className="navbar-item"
                    activeClassName="is-active"
                    activeStyle={activeLinkStyle}
                  >
                    Company
                  </Link>

                  <div className="navbar-dropdown is-right">
                    <Link
                      to="/about/"
                      className="navbar-item"
                      activeStyle={activeLinkStyle}
                    >
                      About Us
                    </Link>
                    <Link
                      to="/#customers-and-partners/"
                      className="navbar-item is-hidden"
                      activeStyle={activeLinkStyle}
                    >
                      Customers and Business Partners
                    </Link>
                  </div>
                </div>
                {!isActive && <span className="navbar-item  is-hidden">|</span>}
                <span className="navbar-item  is-hidden">
                  {!isActive && (
                    <div className="dropdown is-right is-hoverable">
                      <div className="dropdown-trigger search-item">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                        >
                          <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
                        </svg>
                        <span>Search</span>
                      </div>
                      <div
                        className="dropdown-menu"
                        id="dropdown-menu6"
                        role="menu"
                      >
                        <div className="dropdown-content">
                          <div className="dropdown-item">
                            <input
                              className="input"
                              type="text"
                              placeholder="Search Here"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  {isActive && (
                    <input
                      className="input"
                      type="text"
                      placeholder="Search Here"
                    />
                  )}
                </span>
              </div>
            </div>
          </div>
        </Nav>
        {showLiftOff && <LiftOff />}
      </NavWrapper>
    )
  }
}

export default NavBar

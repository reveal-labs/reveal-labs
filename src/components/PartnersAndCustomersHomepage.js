import React, { Component } from 'react'
import styled from 'styled-components'

const Tagline = styled.h3`
  font-size: 32px;
  color: #333;
  font-weight: 400;
  line-height: 1.5;
  text-align: center;
`

class PartnersAndCustomersHomepage extends Component {
  render() {
    return (
      <section className="section has-background-light">
        <div className="container" id="customers-and-partners">
          <Tagline>SOME OF OUR PARTNERS AND CUSTOMERS</Tagline>

          <div className="tile is-ancestor">
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">One</p>
                <p className="subtitle">Subtitle</p>
              </article>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Two</p>
                <p className="subtitle">Subtitle</p>
              </article>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Three</p>
                <p className="subtitle">Subtitle</p>
              </article>
            </div>
            <div className="tile is-parent">
              <article className="tile is-child box">
                <p className="title">Four</p>
                <p className="subtitle">Subtitle</p>
              </article>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default PartnersAndCustomersHomepage

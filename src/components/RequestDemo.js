import React, { Component } from 'react'
import styled from 'styled-components'

import Modal from 'react-responsive-modal'
import RequestDemoModal from './RequestDemoModal'
import {
  validateEmail,
  validEmailPattern,
  validateCorporateEmail,
} from '../utils/helpers'

const FormContainer = styled.div`
  input,
  a {
    height: 44px;
    font-size: 17px;
  }

  #request-email {
    padding: 6px;
    width: 100%;
    border: none;
    border-bottom: 1px solid white;
    background: transparent;
    color: white;
    font-style: italic;
  }

  #submit {
    height: 44px;
    font-weight: bold;
    letter-spacing: 1px;
    border-radius: 3px;
    cursor: pointer;
    text-transform: uppercase;
    &input:focus {
      outline: none !important;
    }
    &input {
      outline-color: rgb(59, 153, 252);
      outline-offset: -2px;
      outline-style: auto;
      outline-width: 5px;
    }
  }
`
export default class RequestDemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      isValid: true,
      email: '',
      errorMessage: '',
    }
    this.onOpenModal = this.onOpenModal.bind(this)
    this.onCloseModal = this.onCloseModal.bind(this)
  }

  onOpenModal() {
    const { email } = this.state
    const isValidEmail = validateEmail(email)
    const isCorporateEmail = validateCorporateEmail(email)

    const isValid = !!(isValidEmail && isCorporateEmail)

    this.setState(prevState => ({
      showModal: isValid,
      isValid,
      errorMessage: !isCorporateEmail
        ? 'Please use a corporate email'
        : prevState.errorMessage,
    }))
  }

  onCloseModal(status) {
    this.setState({
      showModal: false,
    })
  }

  handleAfterOpenFunc() {
    console.log('After opening')
  }

  handleEmailChange = e => {
    const email = e.target.value
    this.setState({ email, isValid: validateEmail(email) })
  }

  onEnterPress = e => (e.key === 'Enter' ? this.onOpenModal() : null)

  render() {
    const { showModal, isValid, email, errorMessage } = this.state
    return (
      <FormContainer id="request-demo">
        <div className="columns">
          <div className="column is-two-thirds">
            <input
              type="email"
              pattern={validEmailPattern}
              id="request-email"
              name="email"
              placeholder="Your Email"
              required
              onChange={this.handleEmailChange}
              onKeyPress={this.onEnterPress}
            />
            <label className={isValid ? 'is-invisible' : 'has-text-danger'}>
              {errorMessage || `Please enter a valid email`}
            </label>
          </div>
          <div className="column">
            <a
              id="submit"
              className="button is-info is-outlined"
              onClick={this.onOpenModal}
            >
              Request Demo
            </a>
          </div>
          <Modal
            open={showModal}
            onClose={this.onCloseModal}
            closeIconSize={14}
            classNames={{ modal: 'request-demo-modal' }}
          >
            <RequestDemoModal email={email} onCloseModal={this.onCloseModal} />
          </Modal>
        </div>
      </FormContainer>
    )
  }
}

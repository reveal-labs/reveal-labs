import React, { Component } from 'react'
import styled from 'styled-components'
import '../styles/mailchimp.classic-10_7.css'
import { toast } from 'react-toastify'
import { validateEmail, validateCorporateEmail } from '../utils/helpers'
// import { ApiService } from '../utils/api-service'
import addToMailchimp from '../../plugins/gatsby-plugin-mailchimp-custom' // 'gatsby-plugin-mailchimp'

const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  .modal-header {
    border-bottom: 1px solid #e5e5e5;
    padding: 8px 8px;
  }
  .modal-body {
    padding: 2px 8px;
  }
  #mc_embed_signup {
    background: #fff;
    clear: left;
    text-align: left;
    input {
      font-size: 14px;
    }

    #mc-embedded-subscribe {
      height: 44px;
      width: 100%;
      margin: 10px 0 10px 0;
      font-weight: bold;
      text-transform: uppercase;
      border-color: ${props => props.theme.colors.revealLightBlue};
    }

    .mc-field-group {
      width: 100%;
      select {
        width: 27%;
      }
    }

    h2 {
      letter-spacing: 0;
      font-weight: normal;
      font-size: 18px;
    }
  }
`

class RequestDemoModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: props.email || '',
      name: '',
      phone: '',
      company: '',
      teamSize: '0 - 30',
      errorMessage: '',
      status: `pending`,
    }
  }

  handleSubmit = e => {
    e.preventDefault()
    e.stopPropagation()
    console.log('submitting form...', this.state)
    const hasInvalidFields = !this.validateFields()

    if (hasInvalidFields) {
      return
    }

    this.setState({ status: `sending` })

    const { email, name, phone, company, teamSize } = this.state

    addToMailchimp(email, {
      fname: name,
      phone,
      compname: company,
      tsize: teamSize,
    })
      .then(response => {
        // Mailchimp always returns a 200 response
        // So we check the result for MC errors & failures
        // If the user already subscribed we currently show a success message
        if (
          response.result === `success` ||
          response.msg.includes('is already subscribed')
        ) {
          this.onSuccess()
        } else {
          this.setState({
            status: `error`,
            errorMessage: response.msg.substring(4),
          })
        }
      })
      .catch(() => {
        // unnecessary because Mailchimp only ever
        // returns a 200 status code
        // TODO: revert to fallback form
        this.setState({ errorMessage: ``, status: `success` })
        this.props.onCloseModal(`success`)
      })
  }

  onSuccess() {
    toast.success('The Demo has been requested successfully.', {
      autoClose: 5000,
      hideProgressBar: true,
      draggable: true,
      position: toast.POSITION.TOP_CENTER,
      draggablePercent: 60,
    })

    this.setState({
      errorMessage: ``,
      status: `success`,
    })
    this.props.onCloseModal(`success`)
  }

  validateFields() {
    const fields = Object.entries(this.state).filter(
      ([key, value]) => key !== 'errorMessage'
    )

    return fields.every(([key, value]) => {
      let err = ''

      if (!value) {
        err = 'Please fill out all fields'
      }

      if (key === 'email') {
        if (!validateEmail(value)) {
          err = 'Invalid email'
        } else if (!validateCorporateEmail(value)) {
          err = 'Please use a corporate email'
        }
      }

      if (err) {
        this.setState({ errorMessage: err })
        return false
      }

      return true
    })
  }

  handleChange = (key, value) => {
    this.setState({
      [key]: value,
    })

    if (this.validateFields()) {
      this.setState({ errorMessage: '', isValid: true })
    }
  }
  render() {
    const { email, name, phone, company, errorMessage, status } = this.state
    return (
      <ModalContent>
        <div className="modal-header">
          <h1 className="title is-3">You're almost done!</h1>
        </div>
        <div className="modal-body">
          <form>
            <div id="mc_embed_signup">
              <div id="mc_embed_signup_scroll">
                <h2 className="subtitle is-2">
                  Just Complete These Final Details to Set-up Your Demo​
                </h2>
                <div className="mc-field-group">
                  <input
                    type="email"
                    value={email}
                    name="EMAIL"
                    required
                    className="required email"
                    id="mce-EMAIL"
                    placeholder="E-Mail Address"
                    onChange={e => this.handleChange('email', e.target.value)}
                  />
                </div>
                <div className="mc-field-group">
                  <input
                    type="text"
                    value={name}
                    name="FNAME"
                    required
                    className=""
                    id="mce-FNAME"
                    placeholder="Name"
                    onChange={e => this.handleChange('name', e.target.value)}
                  />
                </div>
                <div className="mc-field-group">
                  <input
                    type="text"
                    name="PHONE"
                    className=""
                    required
                    value={phone}
                    id="mce-PHONE"
                    placeholder="Phone"
                    onChange={e => this.handleChange('phone', e.target.value)}
                  />
                </div>
                <div className="mc-field-group">
                  <input
                    type="text"
                    value={company}
                    name="COMPNAME"
                    id="mce-COMPNAME"
                    placeholder="Company"
                    onChange={e => this.handleChange('company', e.target.value)}
                  />
                </div>
              </div>
              <div className="mc-field-group">
                <label htmlFor="mce-TSIZE">Team Size</label>
                <select
                  name="TSIZE"
                  className="required"
                  id="mce-TSIZE"
                  onChange={e => this.handleChange('teamSize', e.target.value)}
                >
                  <option value="0 - 30">0 - 30</option>
                  <option value="31 - 100">31 - 100</option>
                  <option value="101 - 500">101 - 500</option>
                  <option value="501 - 1000">501 - 1000</option>
                  <option value="1000+">1000+</option>
                </select>
              </div>
              <div>
                <button
                  type="butotn"
                  value="Request Live Demo"
                  readOnly
                  name="subscribe"
                  id="mc-embedded-subscribe"
                  className={
                    status === `sending`
                      ? 'button is-info is-outlined is-loading'
                      : 'button is-info is-outlined'
                  }
                  onClick={this.handleSubmit}
                >
                  Request Live Demo
                </button>
              </div>
              <label
                className={errorMessage ? 'has-text-danger' : 'is-invisible'}
              >
                {errorMessage}
              </label>
            </div>
          </form>
        </div>
      </ModalContent>
    )
  }
}

export default RequestDemoModal

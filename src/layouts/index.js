import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { ThemeProvider } from 'styled-components'
import config from '../utils/siteConfig'
import 'typeface-roboto'
// import '../styles/global'
import '../styles/custom.scss'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import theme from '../styles/theme'
import Footer from '../components/Footer'
import favicon from '../assets/images/favicon.ico'

class Template extends Component {
  render() {
    const { children } = this.props
    return (
      <div className="has-navbar-fixed-top">
        <Helmet>
          <title>{config.siteTitle}</title>
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
          />
          <link rel="icon" href={favicon} />
          <meta name="description" content={config.siteDescription} />
          <meta property="og:title" content={config.siteTitle} />
          <meta property="og:locale" content="en_US" />
          <meta property="og:type" content="website" />
          <meta property="og:site_name" content={config.siteTitle} />
          <meta property="og:url" content={config.siteUrl} />
        </Helmet>

        <ThemeProvider theme={theme}>
          <div>
            {children()}
            <ToastContainer draggablePercent={60} />
            <Footer />
          </div>
        </ThemeProvider>
      </div>
    )
  }
}

export default Template

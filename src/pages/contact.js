import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'

import config from '../utils/siteConfig'
import ContactForm from '../components/ContactForm'
import Header from '../components/Header'

import contactBanner from '../assets/images/ContactUs.jpeg'
import envelopeIcon from '../assets/images/MAIL_US.svg'
import callsUsIcon from '../assets/images/CALL_US.svg'

const imageMap = {
  'ContactUs.jpeg': contactBanner,
}

const Img = styled.figure`
  margin: auto;
  margin-bottom: 22px;
`

const Contact = ({ data }) => {
  console.log({ data })
  const { file } = data.contentfulAsset

  let bannerImage = imageMap[file.fileName] || file.url

  return (
    <div>
      <Helmet>
        <title>{`Contact - ${config.siteTitle}`}</title>
        <meta property="og:title" content={`Contact - ${config.siteTitle}`} />
        <meta property="og:url" content={`${config.siteUrl}/contact/`} />
      </Helmet>

      <Header title={'Contact Us'} image={bannerImage} />

      <div className="section">
        <div className="container">
          <h2
            className="subtitle is-4 has-text-centered"
            style={{ marginBottom: '50px' }}
          >
            Feel free to drop us a line
          </h2>

          <div className="columns">
            <div className="column has-text-centered is-3 is-offset-3">
              <Img className="image is-48x48 has-text-centered">
                <img src={callsUsIcon} />
              </Img>
              <h4 className="subtitle is-5">Call Us</h4>
              <h6 className="subtitle is-6">{config.contactPhone}</h6>
            </div>

            <div className="column is-3 has-text-centered">
              <Img className="image is-48x48">
                <img src={envelopeIcon} />
              </Img>
              <h4 className="subtitle is-5">Mail Us</h4>
              <h6 className="subtitle is-6">{config.contactEmail}</h6>
            </div>
          </div>
        </div>
      </div>

      <div className="section">
        <div className="container">
          <ContactForm />
        </div>
      </div>
    </div>
  )
}

export default Contact
export const query = graphql`
  query contactQuery {
    contentfulAsset(title: { eq: "ContactUs" }) {
      file {
        url
        fileName
      }
    }
  }
`

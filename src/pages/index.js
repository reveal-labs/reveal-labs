import React, { Component } from 'react'
import styled from 'styled-components'
import Link from 'gatsby-link'
import Header from '../components/Header'
import PartnersAndCustomersHomepage from '../components/PartnersAndCustomersHomepage'

// serve local assets
import cloneIcon from '../assets/images/CLONE_YOUR.svg'
import cxIcon from '../assets/images/CX.svg'
import driveIcon from '../assets/images/DRIVE_GROWTH.svg'
import homePageBanner from '../assets/images/HP.jpg'
import interactionsIcon from '../assets/images/INTERACTIONS.svg'

const iconMap = {
  'CLONE_YOUR.png': cloneIcon,
  'CX.png': cxIcon,
  'DRIVE_GROWTH.png': driveIcon,
  'INTERACTIONS.png': interactionsIcon,
}

const Features = styled.div`
  img {
    height: 61px;
    width: auto;
  }
  h3 {
    margin-bottom: 20px;
    letter-spacing: 1px;
    margin-top: 20px;
  }
  @media (max-width: ${props => props.theme.responsive.mobile}) {
    .feature-box {
      margin-bottom: 12px;
      text-align: center;
      img {
        margin: auto;
      }
      h3 {
        margin-top: 0;
      }
    }
  }
`

const FeaturesSection = styled.section`
  .container {
    width: 80%;
    margin-top: 50px;
    padding-bottom: 60px;
  }
  @media (max-width: ${props => props.theme.responsive.mobile}) {
    .level {
      margin-top: 48px;
    }
  }
`

const Tagline = styled.h3`
  line-height: 1.5;
  text-align: center;
  width: 100%;
  align-self: center;
`

const FeatureBoxes = ({ features }) => (
  <Features className="columns is-multiline">
    {features.map(({ node }, idx) => {
      return (
        <div
          key={node.name}
          id={node.name.replace(/ /g, '-')}
          className="column is-6 feature-box"
        >
          <img src={iconMap[node.icon.file.fileName]} />
          <h3 className="subtitle is-5">{node.name}</h3>
          {node.description.description}
        </div>
      )
    })}
  </Features>
)

const ReadMoreLink = styled.div`
  a {
    font-size: 20px;
    display: flex;
    position: relative;
    align-items: center;
    color: #4a4a4a;
    transition: all 1s ease;
    &:hover {
      color: #17a0e8;
      span {
        color: gray;
        transform: rotate(360deg);
      }
    }
    span {
      right: -20px;
      position: absolute;
      transition: all 1s ease;
      color: #17a0e8;
    }
  }
`

const ReadMore = () => (
  <div className="level">
    <div className="level-right" style={{ width: '100%' }}>
      <div className="level-item">
        <ReadMoreLink title="Learn more about Reveal lab's Face-to-Face Conversation Intelligence Platform">
          <Link to={`/product`} style={{ fontSize: 20 }}>
            Read more <span>→</span>
          </Link>
        </ReadMoreLink>
      </div>
    </div>
  </div>
)

export default class Index extends Component {
  render() {
    const {
      contentfulPage,
      allContentfulHomepageFeatures,
      contentfulDisplayOptions,
    } = this.props.data

    const { headerText, subheaderText, showRequestDemoLink } = contentfulPage
    const { edges: homepageFeatures } = allContentfulHomepageFeatures
    const { show: showPartners } = contentfulDisplayOptions

    return (
      <div>
        <Header
          title={headerText}
          subTitle={subheaderText}
          image={homePageBanner}
          showRequestDemoLink={showRequestDemoLink}
          isLarge
        />

        <FeaturesSection>
          <div className="container">
            <div className="columns is-desktop">
              <div className="column" style={{ display: 'flex' }}>
                <Tagline className="subtitle is-3">
                  Turn Your Frontline Staff <br /> Into CX Rock Stars
                </Tagline>
              </div>

              <div className="column">
                <FeatureBoxes features={homepageFeatures} />
                <ReadMore />
              </div>
            </div>
          </div>
        </FeaturesSection>
        {showPartners && <PartnersAndCustomersHomepage />}
      </div>
    )
  }
}

export const query = graphql`
  query homeQuery {
    contentfulPage(slug: { eq: "home" }) {
      headerText
      subheaderText
      showRequestDemoLink
      bannerImage {
        title
        file {
          url
        }
      }
    }
    allContentfulHomepageFeatures(sort: { fields: [sortOrder] }) {
      edges {
        node {
          name
          description {
            description
          }
          icon {
            file {
              url
              fileName
            }
          }
        }
      }
    }
    contentfulDisplayOptions(name: { eq: "showBusinessPartnersOnHomepage" }) {
      show
    }
  }
`

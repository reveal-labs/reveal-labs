import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import '../styles/privacy_policy.scss'

import config from '../utils/siteConfig'
import Header from '../components/Header'

const MainWrapper = styled.section`
  padding-bottom: 50px;
  padding-top: 30px;
  .border-box {
    border: 1px solid black;
    padding: 4px;
    margin: 6px 0;
    display: block;
  }

  h3 {
    display: inline-block;
    font-family: 'Times New Roman';
  }
`

const PrivacyPolicy = () => {
  return (
    <div>
      <Helmet>
        <title>{`Reveal labs Privacy Policy - ${config.siteTitle}`}</title>
        <meta
          property="og:title"
          content={`Reveal labs Privacy Policy - ${config.siteTitle}`}
        />
        <meta property="og:url" content={`${config.siteUrl}/privacy_policy/`} />
      </Helmet>

      <Header title={'Reveal labs Privacy Policy'} />

      <MainWrapper className="privacy-policy-page">
        <div className="container">
          <p className="c39">
            <span className="c10">Last Revised: July 23, 2018</span>
          </p>
          <p className="c4" id="h.gjdgxs">
            <span className="c3">Reveal Com Ltd. (&ldquo;</span>
            <span className="c6">Reveal</span>
            <span className="c3">&rdquo;, &ldquo;</span>
            <span className="c6">we</span>
            <span className="c3">&rdquo; or &ldquo;</span>
            <span className="c6">us</span>
            <span className="c3">
              &rdquo;) respects the privacy of the users of our Solution (as
              defined below) including those users who were designated by our
              subscribers as employees or personnel who have been authorized to
              use the Solution on their behalf, (the &ldquo;
            </span>
            <span className="c6">Authorized</span>
            <span className="c3">&nbsp;</span>
            <span className="c6">Personnel</span>
            <span className="c3">
              &rdquo;), our subscribers&rsquo; customers or clients (the &ldquo;
            </span>
            <span className="c6">Subscriber Customers</span>
            <span className="c3">
              &rdquo;), and of the users of our website located at:&nbsp;
            </span>
            <span className="c18 c11 c25">
              <a className="c5" href="https://www.reveal-labs.com">
                www.reveal-labs.com
              </a>
            </span>
            <span className="c3">&nbsp; (a &ldquo;</span>
            <span className="c6">Visitors</span>
            <span className="c3">&rdquo; and the &ldquo;</span>
            <span className="c6">Site</span>
            <span className="c3">
              &rdquo;, respectively). We are committed to protect the personal
              information of Visitors, Subscriber Customers and Authorized
              Personnel (each and together &ldquo;
            </span>
            <span className="c6">you</span>
            <span className="c3">&rdquo; or &ldquo;</span>
            <span className="c6">User</span>
            <span className="c3">
              &rdquo;) which we process in connection with the use of our Site
              and Solution (as defined below), as applicable (collectively - the
              &ldquo;
            </span>
            <span className="c6">Services</span>
            <span className="c3">&rdquo;). </span>
          </p>
          <p className="c4" id="h.30j0zll">
            <span className="c3">Reveal provides organizations (&ldquo;</span>
            <span className="c6">Subscribers</span>
            <span className="c3">
              &rdquo;) an in-person interaction video and/or voice recording and
              analytics solution, via desktop and mobile applications (the
              &ldquo;
            </span>
            <span className="c6">App(s)</span>
            <span className="c3">&rdquo; and together the &ldquo;</span>
            <span className="c6">Solution</span>
            <span className="c3">
              &rdquo;) and other associated products and services.{' '}
            </span>
            <span className="border-box">
              STATED SIMPLY, YOU SHOULD BE AWARE THAT IN ACCORDANCE WITH THE
              CONFIGURATION AND CHOICES MADE BY THE SUBSCRIBER (WHO MAY BE THE
              ORGANIZATION YOU WORK FOR, ENGAGED BY OR YOUR SERVICE PROVIDER)
              YOUR IN-PERSON INTERACTIONS WITH CUSTOMERS, USERS OR OTHER
              PERSONNEL OF SUCH SUBSCRIBERS MAY BE RECORDED AND DOCUMENTED BY
              AND VIA THE SOLUTION. REVEAL MERELY PROCESSES THE IN-PERSON
              INTERACTIONS INFORMATION (INCLUDING PERSONAL DATA INCLUDED
              THEREIN) ON BEHALF OF OR IN ACCORDANCE WITH THE INSTRUCTIONS OF
              THE SUBSCRIBER. THE SUBSCRIBER (NOT REVEAL) IS RESPONSIBLE FOR
              PROVIDING YOU, AS WELL AS ALL OTHER PARTICIPANTS OF THE APPLICABLE
              IN-PERSON INTERACTION - WITH ALL LEGAL NECESSARY AND APPROPRIATE
              DISCLOSURES AND NOTICES IN CONNECTION WITH THE SUBSCRIBERS USE OF
              THE SOLUTION (INCLUDING SPECIFICALLY THE VIDEO AND/OR VOICE
              RECORDING). IF FOR ANY REASON YOU FEEL THAT YOUR RECORDING WAS
              OBTAINED IN A MANNER NOT CONSISTENT WITH THIS AGREEMENT AND/OR ANY
              LAWS YOU ARE ENCOURAGED TO CONTACT THE SUBSCRIBER WITH WHOM YOU
              INTERACT.
            </span>
          </p>
          <p className="c4">
            <span className="c3">This Privacy Policy (the &ldquo;</span>
            <span className="c6">Privacy Policy</span>
            <span className="c3">
              &rdquo;) is intended to describe our practices regarding the
              information we may collect from you when you interact or use the
              Services (or any part thereof), the manners in which we may use
              such information, and the options and rights available to you.
            </span>
          </p>
          <p className="c4 c13">
            <span className="c3" />
          </p>
          <ol className="c7 lst-kix_list_2-0 start" start="1">
            <li className="c33">
              <h3>
                <span className="c2">
                  Your Consent (PLEASE READ CAREFULLY!)
                </span>
              </h3>
            </li>
          </ol>
          <p className="c4">
            <span className="c3">
              BY ACCESSING AND/OR USING THE SERVICE (OR ANY PART THEREOF), YOU
              ARE ACCEPTING THE PRACTICES DESCRIBED IN THIS PRIVACY POLICY. BY
              VISITING THE SITE AND/OR INSTALLING AND IMPLEMENTING OR ACCESSING
              THE SOLUTION (INCLUDING THROUGH USE OF THE APPS), YOU AGREE TO THE
              COLLECTION AND PROCESSING OF YOUR PERSONAL INFORMATION (AS DEFINED
              BELOW). IF YOU DISAGREE WITH ANY TERM PROVIDED HEREIN, PLEASE DO
              NOT DOWNLOAD, INSTALL, IMPLEMENT, USE OR ACCESS THE SOLUTION, THE
              SERVICES, THE APPS OR ANY PART THEREOF IN ANY MANNER WHATSOEVER
              AND, IF APPLICABLE, UNINSTALL THE SOLUTION AND THE APPS FROM YOUR
              SYSTEMS AND DO NOT USE IT IN ANY MANNER WHATSOEVER.
            </span>
          </p>
          <p className="c4">
            <span className="c3">
              Please note: you are not obligated by law to provide us with any
              Personal Information.{' '}
            </span>
          </p>
          <p className="c4 c13">
            <span className="c3 c23" />
          </p>
          <ol className="c7 lst-kix_list_2-0" start="2">
            <li className="c33">
              <h3>
                <span className="c2">What Information Do We Collect?</span>
              </h3>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-0 start" start="1">
            <li className="c28 c32">
              <span className="c6">First - What is Personal Information?</span>
            </li>
          </ol>
          <p className="c17">
            <span className="c3">
              Personal Information is individually identifiable information,
              namely information that identifies an individual or may with
              reasonable efforts or together with additional information we have
              access to, enable the identification of an individual, or related
              to an individual or may be of private or sensitive nature
              information relating to an identified or identifiable natural
              person. Identification of an individual also includes association
              of such individual with a persistent identifier such as a name, an
              identification number, persistent cookie identifier etc
            </span>
            <span className="c25 c37">.</span>
          </p>
          <p className="c17">
            <span className="c3">
              Personal Information does not include information that has been
              anonymized or aggregated so that it can no longer be used to
              identify a specific natural person.
            </span>
          </p>
          <ol className="c7 lst-kix_list_3-0" start="2">
            <li className="c0">
              <span className="c6">Different Data Subjects</span>
            </li>
          </ol>
          <p className="c17">
            <span className="c3">
              As reflected above we consider three types of Users
              (data-subjects) with respect to whom we process personal
              information: Visitors, Authorized Personnel, and Subscriber
              Customers. The scope, types and nature of personal information we
              may process in relation to each of these Users-types is different.
              For example, we process very little personal information of
              Visitors to our Site. On the other hand, since the core of our
              Services is all about record-taking of in-person interactions of
              Authorized Personnel with other individuals, this requires us to
              process a greater amount of personal information in relation to
              such Authorized Personnel.{' '}
            </span>
          </p>
          <ol className="c7 lst-kix_list_3-0" start="3">
            <li className="c0">
              <span className="c6">Authorized Personnel Data</span>
            </li>
          </ol>
          <p className="c17">
            <span className="c3">
              We collect and process the following categories of personal
              information in relation to Authorized Personnel:
            </span>
          </p>
          <ol className="c7 lst-kix_list_3-1 start" start="1">
            <li className="c20">
              <span className="c3">
                Data Authorized Personnel actively provide us such as any
                entries, input text, uploaded data or actions in our Solution
                and App, for example:
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-2 start" start="1">
            <li className="c27">
              <span className="c3">
                Authorized Personnel&rsquo;s contact information,{' '}
              </span>
            </li>
            <li className="c27">
              <span className="c3">
                Authorized Personnel&rsquo;s association/employment/affiliation
                with Subscriber, including subordinates and superiors.
              </span>
            </li>
            <li className="c27">
              <span className="c3">
                Authorized Personnel&rsquo;s Solution and system preferences.
              </span>
            </li>
            <li className="c27">
              <span className="c3">
                The contents of your interaction with Subscriber Customers,
                which may include text/video/audio recording and transcripts of
                such communications.{' '}
              </span>
            </li>
            <li className="c27" id="h.1fob9te">
              <span className="c3">
                The contents of your interaction with our customer support,
                which may include text/video/audio recording and transcripts of
                such communications{' '}
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-1" start="2">
            <li className="c20">
              <span className="c3">
                Authorized Personnel&rsquo;s system data such as device
                model/type, web browser version, screen resolution, operating
                system, geolocation (for mobile Apps), etc.
              </span>
            </li>
            <li className="c20">
              <span className="c3">
                Authorized Personnel&rsquo;s Solution and App session data, such
                as IP address, Authorized Personnel&rsquo;s click-stream,
                preferred language, duration of session, time and date of
                session, session replay and session heatmaps, etc.
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-0" start="4">
            <li className="c0">
              <span className="c6">Subscriber Customers Data</span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              We collect and process the following categories of personal
              information in relation to Subscriber Customers:
            </span>
          </p>
          <ol className="c7 lst-kix_list_3-1" start="4">
            <li className="c20">
              <span className="c3">
                Data we are actively provided with by Authorized Personnel via
                the Solution and Apps in connection with the provision of
                Subscriber services, such as any entries, input text, uploaded
                data, or actions, for example:
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-2 start" start="1">
            <li className="c27">
              <span className="c3">
                Subscriber Customer&rsquo;s contact information,
              </span>
            </li>
            <li className="c27">
              <span className="c3">
                Subscriber Customer&rsquo;s account information with respect to
                the services or products purchased from or the account
                maintained with the Subscriber.
              </span>
            </li>
            <li className="c27">
              <span className="c3">
                The contents of your interaction with Subscriber&rsquo;s
                Authorized Personnel, which may include text/video/audio
                recording and transcripts of such communications.
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_3-0" start="5">
            <li className="c0">
              <span className="c6">Visitors Data</span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              We collect and process the following categories of personal
              information in relation to Visitors of our Site:
            </span>
          </p>
          <ol className="c7 lst-kix_list_3-1" start="5">
            <li className="c22">
              <span className="c3">
                Data Visitors actively provide us, such as in an email you may
                send us to our email address as listed on our Site, or any
                entries and text in our Site forms, for example, to our
                &ldquo;contact us&rdquo; form, or &ldquo;request a demo&rdquo;
                form.{' '}
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                Visitors&rsquo; system data such as device model/type, web
                browser version, screen resolution, operating system etc.{' '}
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                Visitors&rsquo; browsing session data such as IP address,
                information we derive from Visitors&rsquo; IP address such as
                city and country associated with the IP address, Visitors&rsquo;
                click-stream, HTTP referral, preferred language, duration of
                session, time and date of session, session replay and session
                heatmaps, etc.
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                The contents of your interaction with our customer support or
                sales departments, which may include text/video/audio recording
                and transcripts of such communications
              </span>
            </li>
          </ol>
          <p className="c28 c13 c41">
            <span className="c3" />
          </p>
          <ol className="c7 lst-kix_list_3-0" start="6">
            <li className="c0">
              <span className="c6">Audio/Video Recording Notice</span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_1-0 start" start="1">
            <li className="c34">
              <span>
                As part the core functionality of the Solution we enable the
                video and/or voice recording of in-person interactions. Such
                recordings, by nature, may capture the voice, image,
                communication and information, of third parties participating in
                such interaction (whether actively or otherwise). You warrant
                and represent that prior to the use of the Solution all
                disclosures necessary or advisable under applicable law have
                been made to all individuals participating and/or in the
                vicinity of such recorded in-person interaction, and all
                consents and approvals for the recording as necessary or
                advisable under applicable law, have been obtained.
              </span>
            </li>
            <li className="c34">
              <h3 id="h.3znysh7">
                <span className="border-box">
                  PLEASE NOTE THAT THE AFOREMENTIONED DISCLOSURE AND CONSENT
                  REQUIREMENTS WITH RESPECT TO RECORDINGS OF IN-PERSON
                  INTERACTION MAY VARY FROM STATE TO STATE, AND COUNTRY TO
                  COUNTRY. YOU REPRESENT, COVENANT, AND WARRANT THAT (I) YOU
                  WILL REVIEW ALL APPLICABLE LAWS, AND TO THE EXTENT REQUIRED,
                  OBTAIN SUCH NEEDED LEGAL COUNSEL BEFORE YOU USE OR ALLOW
                  OTHERS TO USE THE SOLUTION; AND (II) YOUR USE OF THE SOLUTION
                  DOES NOT BREACH OR INFRINGE ON THE RIGHTS OF ANY THIRD PARTY,
                  INCLUDING THE RIGHT FOR PRIVACY. YOU ARE SOLELY RESPONSIBLE
                  FOR COMPLYING WITH ANY AND ALL APPLICABLE LAWS IN CONNECTION
                  WITH YOUR USE OF THE SOLUTION.
                </span>
              </h3>
            </li>
          </ol>
          <p className="c28 c13 c41">
            <span className="c3" />
          </p>
          <ol className="c7 lst-kix_list_2-0" start="3">
            <li className="c12">
              <h3>
                <span className="c2">
                  How Do We Collect Personal Information?
                </span>
              </h3>
            </li>
          </ol>
          <p className="c4 c24">
            <span className="c3">
              There are three main ways in which we collect Personal
              Information: (i) information is actively provided to us by the
              Subscriber or its Authorized Personnel, (ii) information is
              collected as part of the core recording functionality of the
              Solution and otherwise is derived automatically by our systems
              from Authorized Personnel&rsquo;s use of our Solution, Services
              and Apps, (iii) information we obtain from other sources or third
              parties.{' '}
            </span>
          </p>
          <ol className="c7 lst-kix_list_13-0 start" start="1">
            <li className="c0">
              <span className="c6">Actively provided information</span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              Generally, this category refers to any information, data or
              content you actively create, input or provide through our
              Solution, Services or Apps.{' '}
            </span>
          </p>
          <p className="c16">
            <span className="c6">
              PLEASE NOTE - If you give us personal information about someone
              else, you must do so only with that person&rsquo;s express
              authorization. You should inform them how we collect, use,
              disclose, and retain their personal information according to this
              Privacy Policy before you provide us with their personal
              information. Note that voice recordings and video recordings are
              sensitive personal information &nbsp;
            </span>
          </p>
          <ol className="c7 lst-kix_list_13-0" start="2">
            <li className="c0">
              <span className="c6">Automatically collected information</span>
              <span className="c3">&nbsp;</span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              This category refers to personal information automatically
              collected from User&rsquo;s use and interaction with the Solution,
              Services or Apps. We collect information about your interaction
              with our Solution and Apps, in certain cases about your
              impressions of and reactions to our communication with you
              (including newsletters and informational materials about our
              Services and Solution which you agreed to receive from us). This
              is information we receive from devices (including mobile devices)
              and software you use when you access our Solution or Apps.{' '}
            </span>
          </p>
          <p className="c16">
            <span className="c3">
              We perform such automatic collection through use of cookies, web
              beacons, unique identifiers, and similar technologies which allow
              us to collect information about the pages and screens you view,
              the links and objects you click, and other actions you take when
              using our Solution, Services, or Apps, or within our advertising
              or email content, as further detailed in Section 4.{' '}
            </span>
          </p>
          <p className="c16">
            <span className="c3">
              For more information about our use of these technologies and how
              to control them, see&nbsp;our cookies policy &nbsp;
              <a href="https://www.reveal-labs.com/cookies_policy">
                www.reveal-labs.com/cookies_policy
              </a>
            </span>
          </p>
          <ol className="c7 lst-kix_list_13-0" start="3">
            <li className="c0">
              <span className="c3">
                The settings and activation of the recording functionality of
                the Solution is configured by the Subscriber, selecting whether
                such feature is activated in connection with designated business
                hours, calendar events, other criteria, or actively initiated or
                disabled by Authorized Personnel each time it is used to record
                an in-person interaction. For more information regarding the
                activation settings of the recording functionality of the
                Solution, contact the Subscriber (which is an organization
                employing or engaging you).{' '}
              </span>
              <span className="c6">
                Information obtained from third Parties
              </span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              We may also collect personal information concerning you, from
              third parties who have assured us that they have obtained your
              consent for such provision of information or that you have freely
              and publicly provided. For example, if you are an Authorized
              Personnel we will probably be receiving certain information
              relating to you from the Subscriber (which is an organization
              employing or engaging you) in order to provide such Subscriber
              with our Services, e.g., so that we can configure and set up
              Authorized Personnel login credentials for our Solution prior to
              such Authorized Personnel&rsquo;s first login.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="4">
            <li className="c12">
              <h3 id="h.2et92p0">
                <span className="c2">
                  What are the Purposes of the Collection and Processing of
                  Personal Information?
                </span>
              </h3>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_14-0 start" start="1">
            <li className="c0">
              <span className="c6">
                Legal Basis for collection and Processing
              </span>
            </li>
          </ol>
          <p className="c16">
            <span className="c3">
              We collect, process and use Users information for the purposes
              described in this Privacy Policy, based at least on one of the
              following legal grounds:
            </span>
          </p>
          <ol className="c7 lst-kix_list_5-1 start" start="1">
            <li className="c22">
              <span className="c25 c31 c10">
                With respect to Authorized Personnel and Subscriber Customers
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_5-2 start" start="1">
            <li className="c21">
              <span className="c9">For the performance of a contract</span>
              <span className="c3">
                &nbsp;- We collect and process your Personal Information as
                necessary for the establishment and performance of our
                contractual engagements with our Subscriber (who is your
                employer or service provider, respectively), in the scope of our
                role as services providers (processors) to Subscriber and under
                such Subscriber&rsquo;s instructions. Under such engagement we
                shall provide Subscriber with the Services, maintain and improve
                our Services, Solution and Apps, develop new services and
                features for our Users and Subscribers, and personalize the
                Services in order for Users to get a better user experience.
              </span>
            </li>
            <li className="c21">
              <span className="c9">Under our legitimate interest</span>
              <span className="c3">
                - As noted above, we collect and process your Personal
                Information as necessary for the establishment and performance
                of our contractual engagements with our Subscriber (who is your
                employer or service provider, respectively), in the scope of our
                role as services providers (processors) to Subscriber and under
                such Subscriber&rsquo;s instructions. We also we process your
                information for our legitimate interests while applying
                appropriate safeguards that protect your privacy. This means
                that we process your information for things like detecting,
                preventing, or otherwise addressing fraud, abuse, security,
                usability, functionality or technical issues with our Services,
                protecting against harm to the rights, property or safety of our
                properties, our Users, or the public as required or permitted by
                law; enforcing legal claims, including investigation of
                potential violations of this Privacy Policy; in order to comply
                and/or fulfil our obligations under applicable laws,
                regulations, guidelines, industry standards and contractual
                requirements, legal process, subpoena or governmental request,
                as well as our Terms of Use.
              </span>
            </li>
            <li className="c21">
              <span className="c9">Consent</span>
              <span className="c3">
                - We ask for your consent to process certain portions of your
                information for specific purposes, and you have the right to
                withdraw your consent at any time.{' '}
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_5-1" start="2">
            <li className="c22">
              <span className="c25 c10 c31">With respect to Visitors</span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_5-2 start" start="1">
            <li className="c21">
              <span className="c9">For the performance of a contract</span>
              <span className="c3">&nbsp;</span>
              <span className="c9">and provision of services</span>
              <span className="c3">
                &nbsp;- following your acceptance of these Terms, we collect and
                process your personal information in order to provide you with
                our Site; to maintain and improve our Services; to develop new
                services and features for our Users; and to personalize the
                Services in order for you to get a better user experience.
                &nbsp;
              </span>
            </li>
            <li className="c21">
              <span className="c9">Under our legitimate interest</span>
              <span className="c3">
                - We process your information for our legitimate interests while
                applying appropriate safeguards that protect your privacy. This
                means that we process your information for things like
                detecting, preventing, or otherwise addressing fraud, abuse,
                security, usability, functionality or technical issues with our
                Services, protecting against harm to the rights, property or
                safety of our properties, our Users, or the public as required
                or permitted by law; enforcing legal claims, including
                investigation of potential violations of this Privacy Policy; in
                order to comply and/or fulfil our obligations under applicable
                laws, regulations, guidelines, industry standards and
                contractual requirements, legal process, subpoena or
                governmental request, as well as our Terms of Use.{' '}
              </span>
            </li>
            <li className="c21">
              <span className="c9">Consent</span>
              <span className="c3">
                - We ask for your consent to process certain portions of your
                information for specific purposes, and you have the right to
                withdraw your consent at any time. For example, we ask for your
                consent to provide you with personalized content and ads through
                the use of cookies or direct email offers and other marketing
                material. &nbsp;
              </span>
              <span className="c25 c31 c10">&nbsp;</span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_14-0" start="2">
            <li className="c0">
              <span className="c6">
                Why Do We Use Your Personal Information?
              </span>
            </li>
          </ol>
          <p className="c4 c30 c24">
            <span className="c3">
              We may use information that we collect about you, for different
              purposes, which may vary accordingly if you are a Visitor,
              Authorized Personnel, or Subscriber Customer.{' '}
            </span>
          </p>
          <ol className="c7 lst-kix_list_18-0 start" start="1">
            <li className="c1">
              <span className="c3">
                To provide and improve our Services, Solution, Apps and manage
                our business;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To send you updates, notices, notifications, announcements, and
                additional information related to the Services;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To be able to manage your account and provide you with customer
                support;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To display or send you marketing and advertising material and
                general and personalized content and advertisement;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To create cumulative statistical data and other cumulative
                information and/or other conclusive information that is
                non-personal or anonymized, in which we and/or our business
                partners might make use of in order to operate and improve our
                Site, Solution and Apps;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To perform functions or services as otherwise described to you
                at the time of collection;
              </span>
            </li>
            <li className="c1">
              <span className="c3">
                To prevent, detect, mitigate, and investigate fraud, security
                breaches or other potentially prohibited or illegal activities;
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                To comply with any applicable rule or regulation and/or response
                or defend against legal proceedings versus us or our affiliates.{' '}
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_2-0" start="5">
            <li className="c12">
              <h3>
                <span className="c2">
                  Sharing Information with Third Parties
                </span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c10">
              Reveal may share your Personal Information with third parties (or
              otherwise allow them access to it) in a manner and scope{' '}
            </span>
            <span className="c11">
              which may vary accordingly if you are a Visitor, Authorized
              Personnel or Subscriber Customer, and
            </span>
            <span className="c3">&nbsp;only in the following instances:</span>
          </p>
          <ol className="c7 lst-kix_list_15-0 start" start="1">
            <li className="c0">
              <span className="c6">Subscriber:</span>
              <span className="c3">
                &nbsp;We will share certain data concerning Authorized Personnel
                or Subscriber Customer with the Subscriber (their employer or
                service provider, as applicable) as required in the scope of
                provision of Services to such Subscriber.
              </span>
            </li>
            <li className="c0">
              <span className="c6">Third Party Services</span>
              <span className="c3">
                : We are partnering with a number of selected service providers,
                whose services and solutions complement, facilitate and enhance
                our own. These include hosting, database and server co-location
                services (e.g., AWS, Google Cloud, and Azure), data analytics
                services (e.g. Google Analytics), customer relationship
                management solutions and recording platforms (e.g. NICE),
                &nbsp;data and cyber security services, fraud detection and
                prevention services, e-mail and text message distribution and
                monitoring services, payment processors &nbsp;dispute resolution
                providers, customer support and call center services, session
                replay records for app analytic purposes such as crashes,
                functionality and usability, remote access services, and our
                business, legal and financial advisors (collectively, &quot;
              </span>
              <span className="c6">Third Party Service Providers</span>
              <span className="c3">&quot;). </span>
            </li>
          </ol>
          <p className="c4 c24 c30">
            <span className="c10">
              Such Third Party Service Providers may receive or otherwise have
              access to your Personal Information, depending on each of their
              particular roles and purposes in facilitating and enhancing the
              Services, and may only use your Personal Information for such
              purposes. Such disclosure or access is strictly subject to the
              undertaking of confidentiality obligations, and the prevention of
              any independent right to use this information except as required
              to help us provide the Services. We remain responsible and liable
              for any personal information processing done by Third Party
              Service Providers on our behalf, except for events outside of our
              reasonable control and{' '}
            </span>
            <span className="c15 c11">
              except for such Third Party Service Providers with whom you have a
              direct contractual relationship
            </span>
            <span className="c3">. </span>
          </p>
          <ol className="c7 lst-kix_list_15-0" start="3">
            <li className="c0">
              <span className="c6">Other Users</span>
              <span className="c3">
                : if you are an Authorized Personnel or Subscriber Customer, our
                Services may automatically share personal information with the
                Subscriber or select Authorized Personnel to the extent that,
                and in the manner configured by the Subscriber on the Solution.
                Therefore, you are required to take into consideration that the
                use of certain functionality or of input fields, available on
                the Solution, may be accessible to other Authorized Personnel.
                &nbsp; &nbsp;
              </span>
            </li>
            <li className="c0">
              <span className="c19 c15 c11">
                Law Enforcement, Legal Proceedings, and as Authorized by Law
              </span>
              <span className="c14 c11">
                : We may disclose or otherwise allow access to personal
                information pursuant to a legal requirement or request, such as
                a subpoena, search warrant or court order, or in compliance with
                applicable laws and regulations. Such disclosure or access may
                occur with or without notice to you, if we have a good faith
                belief that we are legally required to do so, or that disclosure
                is appropriate in connection with efforts to investigate,
                prevent, or take action regarding actual or suspected illegal
                activity, fraud, or other wrongdoing.
              </span>
            </li>
            <li className="c0">
              <span className="c19 c15 c11">Protecting Rights and Safety</span>
              <span className="c14 c11">
                : We may share your personal information with others, with or
                without notice to you, if we believe in good faith that this
                will help protect our rights, property or personal safety, any
                of our Users, or any members of the general public.
              </span>
            </li>
            <li className="c0">
              <span className="c15 c11 c19">Our Affiliated Companies</span>
              <span className="c14 c11">
                : We may share personal information internally within our family
                of companies, for the purposes described in this Privacy Policy.
                In addition, should we or any of our affiliates undergo any
                change in control, including by means of merger, acquisition or
                purchase of substantially all of our assets, your personal
                information may be shared with the parties involved in such
                event. If we believe that such change in control might
                materially affect your personal information then stored with us,
                we will notify you of this event and the choices you may have
                via e-mail and/or prominent notice on our{' '}
              </span>
              <span className="c3">Site</span>
              <span className="c14 c11">&nbsp;or Services.</span>
            </li>
          </ol>
          <p className="c4 c30 c24">
            <span className="c19 c15 c11">
              For the removal of doubt, we may transfer, share or otherwise use
              non-personal information in our sole discretion and without the
              need for further approval.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="6">
            <li className="c12">
              <h3>
                <span className="c2">
                  Where Do We Store User&rsquo;s Personal Information?
                </span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c14 c11">
              Information regarding the Users may be maintained, processed and
              stored by us and our authorized affiliates and service providers
              in jurisdictions other than the jurisdiction of your residence or
              location, as necessary in secured cloud storage, provided by our
              Third Party Service Providers.{' '}
            </span>
          </p>
          <p className="c4 c8">
            <span className="c14 c11">
              While the data protection laws in such jurisdictions may be
              different than the laws of your residence or location, please know
              that we, our affiliates and our Third Party Service Providers that
              store or process your personal information on our behalf are each
              committed to keep it protected and secured, pursuant to this
              Privacy Policy and industry standards, regardless of any lesser
              legal requirements that may apply in their jurisdiction.
            </span>
          </p>
          <p className="c4 c8">
            <span className="c15 c11">
              For more information regarding the place of storage and the
              transfer of information as described above, please contact us at{' '}
            </span>
            <span className="c18 c11">
              <a className="c5" href="mailto:Privacy@reveal-labs.com">
                Privacy@reveal-labs.com
              </a>
            </span>
            <span className="c11">.</span>
            <span className="c14 c11">&nbsp;</span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="7">
            <li className="c12">
              <h3>
                <span className="c2">
                  Using Cookies and Other Tracking Technologies
                </span>
              </h3>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_16-0 start" start="1">
            <li className="c0">
              <span className="c3">
                We use certain monitoring and tracking technologies, including
                ones offered by third party service providers in order to
                collect and process certain portions of the personal information
                we specified above. These technologies are used in order to
                maintain, provide and improve our Solution and Services on an
                ongoing basis, in order to provide a better experience to our
                Users. For example, these technologies enable us to: (i) keep
                track of and apply our Users&rsquo; Services and Site
                preferences and authenticated sessions, (ii) better secure our
                Solution by detecting abnormal behaviors, (iii) identify
                technical issues and improve the overall performance of our
                Solution and Services, (iv) monitor and analyze our ads&rsquo;
                performance (v) create and monitor analytics relating to use of
                our Solution and Services, and (v) deliver to you targeted
                advertisements that are more tailored to you based on your
                browsing activities and inferred interests.
              </span>
            </li>
          </ol>
          <ul className="c7 lst-kix_list_7-0 start">
            <li className="c4 c38">
              <span className="c6">Cookies</span>
              <span className="c3">
                : A cookie is a small data file that is downloaded and stored on
                your computer or mobile device when you visit our Website. Learn
                more about cookies here:{' '}
              </span>
              <span className="c18 c25 c11">
                <a
                  className="c5"
                  href="https://www.google.com/url?q=http://www.allaboutcookies.org&amp;sa=D&amp;ust=1532430880697000"
                >
                  www.allaboutcookies.org
                </a>
              </span>
            </li>
            <li className="c4 c38">
              <span className="c6">Pixel Tags</span>
              <span className="c3">
                : Pixel tags (also commonly known as web beacons) are
                transparent images, iFrames, or JavaScript placed on our Website
                or our advertisements and emails, that our third party service
                providers use to understand how the Website, such advertisements
                and emails are interacted with.
              </span>
            </li>
          </ul>
          <p className="c4 c30 c24">
            <span className="c3">
              To learn more about our use of Cookies and other tracking
              technology that we use please see our Cookie Policy
              [www.reveal-labs.com/cookies_policy/].
            </span>
          </p>
          <ol className="c7 lst-kix_list_16-0" start="2">
            <li className="c0">
              <span className="c3">
                Some of these tracking technologies are provided to us by our
                Third Party Services Providers who collect and process personal
                information on our behalf. These Third Party Services Providers
                may have direct contractual relationship with you (such as
                Google and Facebook). Additionally, the services provided to us
                by such Third Party Services Providers may entail collection and
                processing of personal information by such Third Party Services
                Providers, in a scope which is broader than the scope of
                personal information we are eventually provided with by such
                Third Party Services Providers. This means that sometimes these
                Third Party Services Providers have more access to your personal
                information than we do. For example, Google Analytics has broad
                access to your information, while we only receive access to
                aggregate statistical information, in order to better protect
                your rights. To the extent you have direct contractual
                relationship with any of our Third Party Services Providers, any
                rights you may have with respect to your information, collected
                by such Third Party Services Providers, shall be governed by
                such contractual relationship. Otherwise the terms of this
                Privacy Policy shall fully apply. &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              </span>
            </li>
            <li className="c0">
              <span className="c6">
                Learn more about your choices and how to opt-out of tracking
                technologies
              </span>
              <span className="c3">:</span>
            </li>
          </ol>
          <p className="c4 c30 c24">
            <span className="c10">
              In order to delete or block any tracking technologies, please
              refer to the &ldquo;Help&rdquo; area on your internet browser for
              further instructions, or learn more by visiting our Cookie Policy
              www.reveal-labs.com/cookies_policy/. You may also opt out of third
              party tracking technologies by following the relevant instructions
              provided by each such third party in its privacy policy or by
              visiting{' '}
            </span>
            <span className="c18 c11">
              <a className="c5" href="https://youronlinechoices.eu/">
                www.youronlinechoices.eu
              </a>
            </span>
            <span className="c18 c11">, </span>
            <span className="c18 c11">
              <a
                className="c5"
                href="http://optout.networkadvertising.org/?c=1#!/"
              >
                optout.networkadvertising.org
              </a>
            </span>
            <span className="c10">&nbsp;or&nbsp;</span>
            <span className="c11 c18">
              <a className="c5" href="https://www.aboutads.info/choices">
                www.aboutads.info/choices/
              </a>
            </span>
            <span className="c10">
              . Please note however that deleting any of our tracking
              technologies or disabling future tracking technologies may prevent
              you from accessing certain areas or features of our Site, or may
              otherwise adversely affect your user experience. Please also note
              that we do not respond to the &lsquo;Do Not Track&rsquo; setting
              on your browser.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="8">
            <li className="c12">
              <h3>
                <span className="c2">Direct Marketing</span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c15 c11">
              You hereby agree that we may use Visitors contact details provided
              during the filling of a contact form on our Site, for the purpose
              of informing you regarding our products and services which may
              interest you, and to send to you other marketing material.{' '}
            </span>
            <span className="c15 c29 c11">
              You may withdraw your consent via sending us written notice by
              email to the following address:{' '}
            </span>
            <span className="c11">Privacy@reveal-labs.com</span>
            <span className="c15 c29 c11">
              &nbsp; or by pressing the &ldquo;Unsubscribe&rdquo; button in the
              mail.
            </span>
          </p>
          <p className="c4 c8">
            <span className="c14 c11">
              Please note that we may also contact Users with important
              information regarding our Solution, Services or Site. For example,
              we may notify you (through any of the means available to us) of
              changes or updates to our Solution, Services or Site, payment
              issues, service maintenance, etc. You will not be able to opt-out
              of receipt of such service messages.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="9">
            <li className="c12">
              <h3>
                <span className="c2">Minors</span>
              </h3>
            </li>
          </ol>
          <p className="c28 c40">
            <span className="c14 c11">
              To use the Services or Solution, you must be over the age of
              sixteen (16). Reveal does not knowingly collect personal
              information from children under the age of sixteen (16) and does
              not wish to do so. We reserve the right to request proof of age at
              any stage so that we can verify that minors under the age of
              sixteen (16) are not using the Services or Solution. In the event
              that it comes to our knowledge that a person under the age of
              sixteen (16) is using the Services or Solution, we will prohibit
              and block such User from accessing the Services or Solution and
              will make all efforts to promptly delete any personal information
              stored with us with regard to such User.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="10">
            <li className="c12">
              <h3>
                <span className="c2">Security </span>
              </h3>
            </li>
          </ol>
          <p className="c8 c28">
            <span className="c11 c15">
              We take a great care in implementing and maintaining the security
              of the Services and Solution, and the User&#39;s personal
              information. The personal information is hosted on Amazon Web
              Services, or upon request by the Subscriber, on Google Cloud, or
              MS-Azure, which provides advanced security features. Reveal
              employs industry standard procedures and policies to ensure the
              safety of its Users&rsquo; personal information and prevent
              unauthorized use of any such information.{' '}
            </span>
            <span className="c3">
              Among other means we use Secure Socket Layer (SSL) technology,
              which creates an encrypted link between our web server and your
              browser, and provides a secured path of communication to ensure
              the information remains protected and private.{' '}
            </span>
          </p>
          <p className="c4 c8">
            <span className="c10">
              Please note however, that regardless of the measures we take and
              the efforts we make, we cannot and do not guarantee the absolute
              protection and security of any personal information.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="11">
            <li className="c12">
              <h3>
                <span className="c2">Data Retention</span>
              </h3>
            </li>
          </ol>
          <p className="c28 c8">
            <span className="c3">
              We may retain your personal information for as long as legitimate
              and to the extent required for the achievement of the purposes
              listed under Section 4, in accordance with the instructions of our
              Subscribers who are the Controllers of the personal data we
              collect and/or process. This includes without limitation, as
              reasonably necessary to comply with our legal obligations and/or
              protect our legitimate interests, and with respect to data used
              for marketing activity, for as long as we have Visitor&rsquo;s
              consent therefor. We then either delete such personal information
              from our systems or anonymize it without further notice to you.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="12">
            <li className="c12">
              <h3>
                <span className="c2">
                  User Rights - Updating, Obtaining a Copy of, or Deleting Your
                  Personal Information
                </span>
              </h3>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_17-0 start" start="1">
            <li className="c0">
              <span className="c19 c15 c11">Visitors</span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_11-1 start" start="1">
            <li className="c22">
              <span className="c14 c11">
                If the law applicable to you grants you such rights, you may ask
                to access, correct, or delete your personal information that is
                stored in our systems. You may also ask for our confirmation as
                to whether or not we process your personal information.
              </span>
            </li>
            <li className="c22">
              <span className="c11 c14">
                Subject to limitations of law, you may request that we update,
                correct, or delete inaccurate or outdated information. You may
                also request that we suspend the use of any personal information
                the accuracy of which you contest while we verify the status of
                that data.
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                Subject the limitations in law, you may also be entitled to
                obtain the Personal Information you directly provided us in a
                structured, commonly used, and machine-readable format and may
                have the right to transmit such data to another party.
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                If you wish to exercise any of these rights or withdraw your
                consent, please contact us at:&nbsp;{' '}
              </span>
              <span className="c18 c25 c11">
                <a className="c5" href="mailto:Privacy@reveal-labs.com">
                  Privacy@reveal-labs.com
                </a>
              </span>
              <span className="c3">
                . When handling these requests, we may ask for additional
                information to confirm your identity and your request. Please
                note, upon request to delete your personal information, we may
                retain such data in whole or in part to comply with any
                applicable rule or regulation and/or response or defend against
                legal proceedings versus us or our affiliates, or as we are
                otherwise permitted under any law applicable to you.
              </span>
            </li>
            <li className="c22">
              <span className="c3">
                To find out whether these rights apply to you and on any other
                privacy related matter, you can contact your local data
                protection authority if you have concerns regarding your rights
                under local law.
              </span>
            </li>
          </ol>
          <ol className="c7 lst-kix_list_17-0" start="2">
            <li className="c0">
              <span className="c6">
                Authorized Personnel and Subscriber Customers
              </span>
            </li>
          </ol>
          <p className="c28 c8">
            <span className="c10">
              As above stated, Reveal collects and processes your personal
              information as a processor (as defined under applicable data
              protection laws and regulations), in the scope of services
              provided by Reveal to a Subscriber which is the employer of
              Authorized Personnel and service provider of Subscriber Customers,
              and in accordance with such Subscriber&rsquo; instructions.
              Therefore, Authorized Personnel and Subscriber Customers may only
              contact Subscriber with any request to exert their rights, claims
              or inquires with respect to any of their personal information,
              which has been collected and processed hereunder.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="13">
            <li className="c12">
              <h3>
                <span className="c2">Changes to the Privacy Policy </span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c3">
              Reveal reserves the right to change this Privacy Policy at any
              time and at its sole discretion, so please re-visit this page
              frequently. We will provide notice of substantial changes of this
              Privacy Policy on the Solution, the Site, the App and/or by
              sending you an e-mail to the e-mail address that you provided us.
              Substantial changes will take effect seven (7) days after such
              notice was provided on any of the abovementioned methods.
              Otherwise, all other changes to this Privacy Policy are effective
              as of the stated &ldquo;Last Revised&rdquo; date, and in any event
              your continued use of the Services after the Last Revised date or
              after the date of email notice, will constitute acceptance of, and
              agreement to be bound by, those changes.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="14">
            <li className="c12">
              <h3>
                <span className="c2">General Information</span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c3">
              This Privacy Policy, its interpretation, and any claims and
              disputes related hereto, shall be governed by the laws of the
              State of Israel, without respect to its criminal law principles.
              Any and all such claims and disputes shall be brought in, and you
              hereby consent to them being litigated in and decided exclusively
              by a court of competent jurisdiction located in Tel Aviv, Israel.
            </span>
          </p>
          <ol className="c7 lst-kix_list_2-0" start="15">
            <li className="c12">
              <h3>
                <span className="c31 c29 c11">Have any Questions</span>
                <span className="c11 c29">?</span>
              </h3>
            </li>
          </ol>
          <p className="c4 c8">
            <span className="c10">
              If you have any questions (or comments) concerning this Privacy
              Policy, you are welcome to send us an e-mail at
            </span>
            <span className="c11">: </span>
            <span className="c18 c11">
              <a className="c5" href="mailto:Privacy@reveal-labs.com">
                Privacy@reveal-labs.com
              </a>
            </span>
            <span className="c11">&nbsp;</span>
            <span className="c10">and we will make an effort to</span>
            <span className="c10 c29">&nbsp;</span>
            <span className="c3">reply within a reasonable timeframe.</span>
          </p>
        </div>
      </MainWrapper>
    </div>
  )
}

export default PrivacyPolicy

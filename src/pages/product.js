import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import Header from '../components/Header'
import config from '../utils/siteConfig'
import ContactForm from '../components/ContactForm'

// serve local assets
import cloneIcon from '../assets/images/CLONE_YOUR.svg'
import cxIcon from '../assets/images/CX.svg'
import driveIcon from '../assets/images/DRIVE_GROWTH.svg'
import interactionsIcon from '../assets/images/INTERACTIONS.svg'
import unlockIcon from '../assets/images/UNLOCK_INSIGHTS.svg'
import improveQualityIcon from '../assets/images/IMPROVE_QUALITY.svg'
import thirdPartyIcon from '../assets/images/3RD_PARTY.svg'
import voiceIcon from '../assets/images/VIDEO&VOICE.svg'
import conversationAnalysisIcon from '../assets/images/CONVERSATION_ANALYSIS.svg'
import callsUsIcon from '../assets/images/CALL_US.svg'
import multiPlatformIcon from '../assets/images/MULTI-PLATFORM.svg'
import productBanner from '../assets/images/Product1.jpg'

const iconMap = {
  'CLONE_YOUR.png': cloneIcon,
  'CX.png': cxIcon,
  'DRIVE_GROWTH.png': driveIcon,
  'INTERACTIONS.png': interactionsIcon,
  'UNLOCK_INSIGHTS.png': unlockIcon,
  'IMPROVE_QUALITY.png': improveQualityIcon,
  'VIDEO&VOICE.png': voiceIcon,
  '3RD_PARTY.png': thirdPartyIcon,
  'CONVERSATION_ANALYSIS.png': conversationAnalysisIcon,
  'CALL_US.png': callsUsIcon,
  'MULTI-PLATFORM.png': multiPlatformIcon,
}

const TopFeaturesContainer = styled.div`
  img {
    height: 61px;
    width: auto;
  }
  h3 {
    margin-bottom: 20px;
    letter-spacing: 1px;
    margin-top: 20px;
    font-size: 18px;
  }

  @media (max-width: ${props => props.theme.responsive.mobile}) {
    img {
      margin-bottom: 0;
    }
    h3 {
      margin-top: 0;
    }

    .column {
      margin-bottom: 6px;
    }
  }
`

const MainFeaturesContainer = styled.div`
  img {
    height: 61px;
    width: auto;
  }
  h3 {
    margin-bottom: 20px;
    letter-spacing: 1px;
    margin-top: 20px;
    font-size: 18px;
  }
  h2 {
    margin-top: 70px !important;
    margin-bottom: 100px !important;
  }
  @media (max-width: ${props => props.theme.responsive.mobile}) {
    h2 {
      margin-top: 0 !important;
      margin-bottom: 30px !important;
    }
  }
`

const Section = styled.section`
  p {
    margin: 0 0 30px;
    color: #7e7e7e;
    line-height: 30px;
  }
`

const FormSectionEl = styled.div`
  h3 {
    margin-bottom: 20px;
    letter-spacing: 1px;
    margin-top: 20px;
    font-size: 18px;
  }
`

const IntroSection = ({ body }) => (
  <Section className="section" style={{ paddingBottom: 0 }}>
    <div
      className="container has-text-centered is-size-5"
      dangerouslySetInnerHTML={{ __html: body.childMarkdownRemark.html }}
    />
  </Section>
)

const TopFeatures = ({ features }) => (
  <Section className="section">
    <TopFeaturesContainer className="container">
      <div className="columns is-multiline">
        {features.map(feature => {
          return (
            <div
              key={feature.name}
              id={feature.name.replace(/ /g, '-')}
              className="column is-4 has-text-centered"
            >
              <img
                src={
                  iconMap[feature.icon.file.fileName] || feature.icon.file.url
                }
              />
              <h3 className="subtitle is-uppercase">{feature.name}</h3>
              {feature.description.description}
            </div>
          )
        })}
      </div>
    </TopFeaturesContainer>
  </Section>
)

const MainFeatures = ({ features }) => (
  <Section className="section custom-gray-bg">
    <MainFeaturesContainer className="container">
      <h2 className="has-text-centered subtitle is-4 is-uppercase">Features</h2>
      <div className="columns is-multiline">
        {features.map(feature => {
          return (
            <div
              key={feature.name}
              id={feature.name.replace(/ /g, '-')}
              className="column is-6"
            >
              <article className="media">
                <figure className="media-left">
                  <p className="image">
                    <img
                      src={
                        iconMap[feature.icon.file.fileName] ||
                        feature.icon.file.url
                      }
                    />
                  </p>
                </figure>
                <div className="media-content">
                  <h3 className="subtitle is-uppercase">{feature.name}</h3>
                  <div className="content" style={{ padding: '0 40px 0 0' }}>
                    <p>{feature.description.description}</p>
                  </div>
                </div>
              </article>
            </div>
          )
        })}
      </div>
    </MainFeaturesContainer>
  </Section>
)

const FormSection = () => (
  <FormSectionEl className="section">
    <div className="container">
      <h3 className="subtitle has-text-centered is-uppercase">
        Interested in recording your face-to-face interactions?
      </h3>
      <h4 className="subtitle has-text-centered is-6">Drop us a line</h4>
      <ContactForm />
    </div>
  </FormSectionEl>
)

export default class Product extends Component {
  render() {
    const { contentfulPage } = this.props.data

    const {
      title,
      slug,
      body,
      headerText,
      subheaderText,
      showRequestDemoLink,
      bannerImage,
      features,
    } = contentfulPage

    const sortedFeatures = features.sort((a, b) => a.sortOrder - b.sortOrder)

    return (
      <Fragment>
        <Helmet>
          <title>{`${title} - ${config.siteTitle}`}</title>
          <meta
            property="og:title"
            content={`${title} - ${config.siteTitle}`}
          />
          <meta property="og:url" content={`${config.siteUrl}/${slug}/`} />
        </Helmet>
        <Header
          title={headerText}
          subTitle={subheaderText}
          image={productBanner || (bannerImage && bannerImage.file.url)}
          showRequestDemoLink={showRequestDemoLink}
          isLarge={slug === 'product'}
          customClass={`${slug}-header`}
        />

        <IntroSection body={body} />
        <TopFeatures features={sortedFeatures.slice(0, 3)} />
        <MainFeatures features={sortedFeatures.slice(3)} />
        <FormSection />
      </Fragment>
    )
  }
}

export const query = graphql`
  query productQuery {
    contentfulPage(slug: { eq: "product" }) {
      title
      slug
      body {
        childMarkdownRemark {
          html
        }
      }
      headerText
      subheaderText
      showRequestDemoLink
      features {
        sortOrder
        name
        description {
          id
          description
        }
        icon {
          file {
            url
            fileName
            contentType
          }
        }
      }
      bannerImage {
        title
        file {
          url
        }
      }
    }
  }
`

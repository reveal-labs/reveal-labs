import styled from 'styled-components'

const theme = {
  colors: {
    base: '#121212', // Black
    secondary: '#e9e9e9', // Medium Gray
    tertiary: '#f3f3f3', // Light Gray
    highlight: '#5b8bf7', // Light Blue
    white: 'white',
    revealBlue: '#0E3E59',
    revealDarkBlue: '#0D2C54',
    revealLightBlue: '#17A0E8',
    revealGreen: '#20bf55',
  },
  sizes: {
    maxWidth: '1200px',
    maxWidthCentered: '650px',
  },
  responsive: {
    small: '35em',
    medium: '50em',
    large: '70em',
    mobile: '768px',
  },
}

export default theme

export const Section = styled.section`
  p {
    margin: 0 0 30px;
    color: #7e7e7e;
    line-height: 30px;
  }
`
export const TagLink = styled.a`
  margin-right: 10px;
  transition: background-color 0.3s;
  &:hover {
    a {
      color: #fff;
    }
    background: #1a2e61;
    border: 1px solid #1a2e61;
  }
`

import React from 'react'
import Helmet from 'react-helmet'
import config from '../utils/siteConfig'
import styled from 'styled-components'
import Header from '../components/Header'
import ContactForm from '../components/ContactForm'
import { Section } from '../styles/theme'

const FormSectionEl = styled.section`
  h3 {
    margin-bottom: 20px;
    letter-spacing: 1px;
    margin-top: 20px;
    font-size: 24px;
  }
`

const FormSection = () => (
  <FormSectionEl className="section custom-gray-bg">
    <div className="container">
      <h3 className="subtitle has-text-centered is-uppercase">Contact Us</h3>
      <h4 className="subtitle has-text-centered is-6">Drop us a line</h4>
      <ContactForm />
    </div>
  </FormSectionEl>
)

const PageTemplate = ({ data }) => {
  const {
    title,
    slug,
    body,
    headerText,
    subheaderText,
    showContactForm,
    showRequestDemoLink,
    bannerImage,
  } = data.contentfulPage

  return (
    <div>
      <Helmet>
        <title>{`${title} - ${config.siteTitle}`}</title>
        <meta property="og:title" content={`${title} - ${config.siteTitle}`} />
        <meta property="og:url" content={`${config.siteUrl}/${slug}/`} />
      </Helmet>

      <Header
        title={headerText}
        subTitle={subheaderText}
        image={bannerImage && bannerImage.file.url}
        showRequestDemoLink={showRequestDemoLink}
        isLarge={slug === 'product'}
        customClass={`${slug}-header`}
      />

      <Section className="section">
        <div
          className="container"
          dangerouslySetInnerHTML={{ __html: body.childMarkdownRemark.html }}
        />
      </Section>
      {showContactForm ? <FormSection /> : null}
    </div>
  )
}

export const query = graphql`
  query pageQuery($slug: String!) {
    contentfulPage(slug: { eq: $slug }) {
      title
      slug
      body {
        childMarkdownRemark {
          html
        }
      }
      headerText
      subheaderText
      showContactForm
      showRequestDemoLink
      bannerImage {
        title
        file {
          url
        }
      }
    }
  }
`

export default PageTemplate

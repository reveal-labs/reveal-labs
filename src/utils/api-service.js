import axios from 'axios'
const encode = encodeURIComponent

export class ApiService {
  static mailchimpDemoRequest(fields) {
    const endpoint =
      'https://iron-buzz.us17.list-manage.com/subscribe/post-json?u=b5b9d576cf10499ff8a820a58&id=d43cc8c370&'

    const email = encode(fields.email)
    const name = encode(fields.name)
    const phone = encode(fields.phone)
    const company = encode(fields.company)
    const teamSize = encode(fields.teamSize)

    const data = `EMAIL=${email}&FNAME=${name}&PHONE=${phone}&COMPNAME=${company}&TSIZE=${teamSize}&b_b5b9d576cf10499ff8a820a58_d43cc8c370=`
    const url = `${endpoint}${data}`

    // const tmp =
    //   'https://iron-buzz.us17.list-manage.com/subscribe/post-json?u=b5b9d576cf10499ff8a820a58&id=d43cc8c370&EMAIL=jonathanl-testing1129%40iron-buzz.com&FNAME=Jon+Testing&PHONE=1234&COMPNAME=Testing+Co.&TSIZE=501+-+1000&b_b5b9d576cf10499ff8a820a58_d43cc8c370=&b_e44c1f194bec93e238615469e_f6f826e769=&_=1525881247893&c=__jp0'
    // https://iron-buzz.us17.list-manage.com/subscribe/post-json?u=b5b9d576cf10499ff8a820a58&id=d43cc8c370&EMAIL=jonathanl1118%40iron-buzz.com&0=Jon%20Tests&1=123&2=RL&3=101%20-%20500&c=__jp0
    return axios({
      method: 'get',
      url: url,
    })
  }
}

// Email validation helpers
export const validEmailPattern = `[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,63}$`

export const validateEmail = email =>
  email && email.length > 0 && email.match(validEmailPattern)

const invalidHosts = ['gmail', 'hotmail', 'yahoo']

export const validateCorporateEmail = email =>
  invalidHosts.every(host => !email.includes(host))

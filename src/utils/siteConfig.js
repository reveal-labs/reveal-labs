module.exports = {
  siteTitle: 'Reveal labs',
  siteDescription:
    'Innovative Face-to-Face Conversation Intelligence Platform for Sales and &lt;span&gt;CUSTOMER EXPERIENCE PROFESSIONALS&lt;/span&gt;',
  siteUrl: 'httsp://reveal-labs.com', // Site domain. Do not inculde a trailing slash! If you wish to use a path prefix you can read more about that here: https://www.gatsbyjs.org/docs/path-prefix/
  contactEmail: 'info@reveal-labs.com',
  contactPhone: '+1 (646) 779 7020',
}

import Typography from 'typography'

const typography = new Typography({
  title: 'reveal',
  googleFonts: [
    {
      name: 'Roboto',
      styles: ['300', '300i', '400', '400i', '500', '500i', '700', '700i'],
    },
  ],
  baseFontSize: '18px',
  baseLineHeight: 1.45,
  headerFontFamily: [
    'Roboto',
    'Helvetica Neue',
    'Segoe UI',
    'Helvetica',
    'Arial',
    'sans-serif',
  ],
  bodyFontFamily: ['Roboto', 'Georgia', 'serif'],
})

export default typography
